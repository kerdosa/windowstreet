/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * Defines express middlewares used in the application.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


require('./function-utils');

var winston = require('winston'),
  errors = require('../errors'),
  constant = require('../helpers/constant');

/**
 * This method check if current user is admin.
 * Note that generic "requiresAdmin" from auth module is not used because I want to include
 * error messages in JSON payload instead of plain-text.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Funciton} next the next function in the chain
 */
function requireAdmin(req, res, next) {
  if (!req.user || (req.user.role !== 'Admin')) {
    next(new errors.ForbiddenError('You are not authorized to perform the requested operation.'));
  } else {
    next();
  }
}

/**
 * The middleware for error handling
 * @param {Error} err the occurred error
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function errorHandler(err, req, res, next) {//jshint ignore:line
  // log the stack trace
  winston.error(err.stack);
  res.status(err.httpCode || constant.HTTP_INTERNAL_SERVER_ERROR).json({
    message: err.message || 'failed to process a request'
  });
}

module.exports = {
  requireAdmin: requireAdmin,
  errorHandler: errorHandler
};