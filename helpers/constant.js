/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * Defines application constants.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

module.exports = {
  // user role type
  ROLE_ADMIN: 'Admin',
  ROLE_ENDOWMENT: 'Endowment',
  
  // http status code
  HTTP_OK: 200,
  HTTP_CREATED: 201,
  HTTP_INTERNAL_SERVER_ERROR: 500,
  HTTP_BAD_REQUEST: 400,
  HTTP_UNAUTHORIZED: 401,
  HTTP_FORBIDDEN: 403,
  HTTP_NOT_FOUND: 404,
  HTTP_NO_CONTENT: 204
};