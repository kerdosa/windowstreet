/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This contains helper methods.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var config = require('config');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var async = require('async');
var path = require('path');
var fs = require('fs');
var ejs = require('ejs');

//directory for email templates
var _templatesDir  = path.join(__dirname, '../config/emailTemplates');

//smtp client
var _transporter = nodemailer.createTransport(config.MAIL);

//function delegate for email templates
var _templates = {};

//exported helper
var helper = {};

/**
 * Read email template (subject, text and html content)
 * @param {String} templateName the template name
 * @param {Function<err, result>} callback the callback function
 * @private
 */
function _readTemplate(templateName, callback) {
    var basePath = path.join(_templatesDir, templateName);
    var readFile = function (name) {
        return function (cb) {
            fs.readFile(path.join(basePath, name), 'utf8', cb);
        };
    };
    async.parallel({
        html: readFile('html.ejs'),
        text: readFile('text.ejs'),
        subject: readFile('subject.ejs')
    }, callback);
}

/**
 * Template email data with given values
 * @param {String} templateName the template name
 * @param {Object} values the values of email template
 * @param {Function<err, result>} callback the callback function
 * @private
 */
function _templateEmail(templateName, values, callback) {
    async.waterfall([
        function (cb) {
            if (_templates[templateName]) {
                cb();
            } else {
                _readTemplate(templateName, function (err, template) {
                    if (err) {
                        cb(err);
                    } else {
                        _templates[templateName] = template;
                        cb();
                    }
                });
            }
        }, function (cb) {
            var template = _templates[templateName];
            var result = {
                html: ejs.render(template.html, values),
                text: ejs.render(template.text, values),
                subject: ejs.render(template.subject, values)
            };
            cb(null, result);
        }
    ], callback);
}

/**
 * Hash a password.
 * @param {String} password the password to hash
 * @param {Function<err, hash>} callback the callback function
 */
helper.hashPassword = function (password, callback) {
    crypto.pbkdf2(password, config.SECURITY.salt, config.SECURITY.iterations, config.SECURITY.keylen, function (err, pass) {
        if (err) {
            callback(err);
        } else {
            var buff = new Buffer(pass, 'binary');
            callback(null, buff.toString('hex'));
        }
    });
};

/**
 * Hash a password synchronlusly.
 * @param {String} password the password to hash
 * @return {String} the password hash
 * @private
 */
helper.hashPasswordSync = function(password) {
    var pass = crypto.pbkdf2Sync(password, config.SECURITY.salt, config.SECURITY.iterations, config.SECURITY.keylen);
    return new Buffer(pass, 'binary').toString('hex');
};

/**
 * Random short password
 * @param {Function<err, password>} callback the callback function
 */
helper.randomPassword = function (callback) {
    crypto.randomBytes(Math.round(config.RANDOM_PASSWORD_LEN / 2), function (err, buf) {
        if (err) {
            callback(err);
        } else {
            callback(null, buf.toString('hex'));
        }
    });
};


/**
 * Send a email.
 * @param {String} from the receiver email
 * @param {String} to the sender email
 * @param {String} templateName the template name to send
 * @param {Object} values the values of email template
 * @param {Function<err>} callback the callback function
 */
helper.sendEmail = function (from, to, templateName, values, callback) {
    async.waterfall([
        function (cb) {
            _templateEmail(templateName, values, cb);
        }, function (template, cb) {
            _transporter.sendMail({
                from: from,
                to: to,
                subject: template.subject,
                html: template.html,
                text: template.text
            }, cb);
        }
    ], function (err) {
        callback(err);
    });
};

module.exports = helper;