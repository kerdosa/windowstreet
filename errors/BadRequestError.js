/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
/**
 * This file defines BadRequestError
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

/**
 * Constructor of BadRequestError
 * @param {Object} message the error message
 * @param {Object} cause the error cause
 */
var BadRequestError = function (message, cause) {
    //captureStackTrace
    Error.call(this);
    Error.captureStackTrace(this);
    this.message = message || 'BadRequestError';
    this.httpCode = 400;
    this.cause = cause;
};

//use Error as prototype
require('util').inherits(BadRequestError, Error);
BadRequestError.prototype.name = 'BadRequestError';

module.exports = BadRequestError;
