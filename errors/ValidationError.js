/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
/**
 * This file defines ValidationError
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

/**
 * Constructor of ValidationError
 * @param {Object} message the error message
 * @param {Object} cause the error cause
 */
function ValidationError(message, cause) {
    Error.call(this);
    Error.captureStackTrace(this);
    this.message = message || 'ValidationError';
    this.cause = cause;
    this.httpCode = 400;
}

//use Error as prototype
require('util').inherits(ValidationError, Error);
ValidationError.prototype.name = 'ValidationError';

module.exports = ValidationError;