/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
/**
 * This file defines InternalServerError
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


/**
 * Constructor of InternalServerError
 * @param {Object} message the error message
 * @param {Object} cause the error cause
 */
function InternalServerError(message) {
  // capture stack trace
  Error.call(this);
  Error.captureStackTrace(this);
  this.name = 'InternalServerError';
  this.message = message;
  this.code = 500;
}

require('util').inherits(InternalServerError, Error);
InternalServerError.prototype.name = 'InternalServerError';

module.exports = InternalServerError;