/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
/**
 * This file defines ForbiddenError
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

/**
 * Constructor of ForbiddenError
 * @param {Object} message the error message
 * @param {Object} cause the error cause
 */
var ForbiddenError = function (message, cause) {
    //captureStackTrace
    Error.call(this);
    Error.captureStackTrace(this);
    this.message = message || 'ForbiddenError';
    this.httpCode = 403;
    this.cause = cause;
};

//use Error as prototype
require('util').inherits(ForbiddenError, Error);
ForbiddenError.prototype.name = 'ForbiddenError';

module.exports = ForbiddenError;
