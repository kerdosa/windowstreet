/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
/**
 * Defines all Error objects in this folder.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


module.exports = {
  ValidationError: require('./ValidationError'),
  NotFoundError: require('./NotFoundError'),
  BadRequestError: require('./BadRequestError'),
  ForbiddenError: require('./ForbiddenError'),
  IllegalArgumentError: require('./IllegalArgumentError'),
  UnauthorizedError: require('./UnauthorizedError')
};