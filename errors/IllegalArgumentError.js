/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
/**
 * This file defines IllegalArgumentError
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

/**
 * Constructor of IllegalArgumentError
 * @param {Object} message the error message
 * @param {Object} cause the error cause
 */
var IllegalArgumentError = function (message, cause) {
    //captureStackTrace
    Error.call(this);
    Error.captureStackTrace(this);
    this.message = message || 'IllegalArgumentError';
    this.httpCode = 400;
    this.cause = cause;
};

//use Error as prototype
require('util').inherits(IllegalArgumentError, Error);
IllegalArgumentError.prototype.name = 'IllegalArgumentError';

module.exports = IllegalArgumentError;
