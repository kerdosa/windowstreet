/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
/**
 * This file defines NotFoundError
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

/**
 * Constructor of NotFoundError
 * @param {Object} message the error message
 * @param {Object} cause the error cause
 */
var NotFoundError = function (message, cause) {
    //captureStackTrace
    Error.call(this);
    Error.captureStackTrace(this);
    this.message = message || 'NotFoundError';
    this.httpCode = 404;
    this.cause = cause;
};

//use Error as prototype
require('util').inherits(NotFoundError, Error);
NotFoundError.prototype.name = 'NotFoundError';

module.exports = NotFoundError;