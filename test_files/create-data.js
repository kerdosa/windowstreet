/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * Create test data to DB.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

// set node-config directory environment
process.env.NODE_CONFIG_DIR = __dirname + '/../config';


// bootstrap mongodb connection and models
require('../models');

var async = require('async'),
  _ = require('lodash'),
  moment = require('moment'),
  helper = require('./dataHelper'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Endowment = mongoose.model('Endowment'),
  Borrower = mongoose.model('Borrower'),
  Installer = mongoose.model('Installer'),
  SystemModel = mongoose.model('System'),
  SystemPerformance = mongoose.model('SystemPerformance'),
  Loan = mongoose.model('Loan'),
  LoanPayment = mongoose.model('LoanPayment');


var users = [{
    '_id': '100000000000000000000001',
    'email': 'admin@gmail.com',
    'passwordHash': 'bedbe72c9368ba5b957783182713ae05', // the password is 'password'
    'role': 'Admin'
  },
  {
    '_id': '100000000000000000000002',
    'email': 'topcoder3@gmail.com',
    'passwordHash': 'bedbe72c9368ba5b957783182713ae05',
    'role': 'Endowment'
  },
  {
    '_id': '100000000000000000000003',
    'email': 'topcoder70@gmail.com',
    'passwordHash': 'bedbe72c9368ba5b957783182713ae05',
    'role': 'Endowment'
  }];

var endowmentUsers = _.filter(users, function (user) {
  if (user.role === 'Endowment') {
    return user;
  }
});

// Don't increase too much, then it reaches out of memory or max number of loop.
// If you get out of memory, decrease the number in NUM_SYSTEMS_PER_INSTALLER, NUM_SYSTEM_WITH_DATA or YEARS.
var NUM_INSTALLERS = 6, NUM_BORROWERS_PER_ENDOWMENT = 1, NUM_LOANS_PER_BORROWER = 50, 
  NUM_LOAN_PAYMENTS_PER_LOAN = 10, NUM_SYSTEMS_PER_INSTALLER = 15, NUM_SYSTEM_WITH_DATA = 5, YEARS = 7;

var endowments = [], borrowers = [], loans = [], installers = [];

async.series([
  function (cb) {
    User.remove({}, cb);
  },
  function (cb) {
    Endowment.remove({}, cb);
  },
  function (cb) {
    Borrower.remove({}, cb);
  },
  function (cb) {
    Installer.remove({}, cb);
  },
  function (cb) {
    SystemModel.remove({}, cb);
  },
  function (cb) {
    SystemPerformance.remove({}, cb);
  },
  function (cb) {
    Loan.remove({}, cb);
  },
  function (cb) {
    LoanPayment.remove({}, cb);
  },
  function (cb) {   // create users
    console.log('creating users');
    User.create(users, cb);
  },
  function (cb) {   // create installers
    console.log('creating installers');
    async.times(NUM_INSTALLERS, function (n, cb2) {
      Installer.create(helper.createInstaller(n), function (err, installer) {
        installers.push(installer);
        cb2(err);
      });
    }, cb);
  },
  function (cb) {   // create endowments
    console.log('creating endowments');
    async.times(endowmentUsers.length, function (n, cb2) {
      async.times(NUM_BORROWERS_PER_ENDOWMENT, function (k, cb3) {
        Endowment.create(helper.createEndowment(n*endowmentUsers.length+k, endowmentUsers[n%endowmentUsers.length]), function (err, endowment) {
          endowments.push(endowment);
          cb3(err);
        });
      }, cb2);
    }, cb);
  },
  function (cb) {   // create borrowers
    console.log('creating borrowers');
    async.times(endowments.length, function (n, cb2) {
      Borrower.create(helper.createBorrower(n, endowments[n]), function (err, borrower) {
        borrowers.push(borrower);
        cb2();
      });
    }, cb);
  },
  function (cb) {   // create loans
    console.log('creating loans');
    async.times(borrowers.length, function (n, cb2) {
      async.times(NUM_LOANS_PER_BORROWER, function (k, cb3) {
        Loan.create(helper.createLoan(n*borrowers.length+k, borrowers[n]), function (err, loan) {
          loans.push(loan);
          cb3(err);
        });
      }, cb2);
    }, cb);
  },
  function (cb) {   // create loanPayments
    console.log('creating loan payments');
    async.times(loans.length, function (n, cb2) {
      async.times(NUM_LOAN_PAYMENTS_PER_LOAN, function (k, cb3) {
        LoanPayment.create(helper.createLoanPayment(n*loans.length+k, loans[n]), function (err, loan) {
          cb3(err, loan);
        });
      }, cb2);
    }, cb);
  },
  function (cb) {   // create systems
    console.log('creating systems');
    async.times(installers.length, function (n, cb2) {
      async.times(NUM_SYSTEMS_PER_INSTALLER, function (k, cb3) {
        SystemModel.create(helper.createSystem(n*installers.length+k, installers[n], borrowers[n%borrowers.length]), function (err, system) {
          cb3(err, system);
        });
      }, cb2);
    }, cb);
  },
  function (cb) {   // create systemPerformances
    console.log('creating system performances');
    async.each(borrowers, function (borrower, cb2) {
      SystemModel.find({borrower: borrower._id}, function (err, systems) {
        async.times(systems.length, function (n, cb3) {
          var today = moment().startOf('day');
          // create data for past 7 years for first 5 systems for each borrower to avoid out of memoty, one data / day
          var years;
          if (n < NUM_SYSTEM_WITH_DATA) {
            years = YEARS;
            console.log('creating ...', n, systems[n].borrower, systems[n]._id);
            async.times(365*years, function (i, cb4) {
              today.subtract(1, 'days');
              SystemPerformance.create(helper.createSystemPerformance(today, systems[n]), cb4);
            }, cb3);
          } else {
            cb3();
          }
        }, cb2);
      });
    }, cb);
  }
], function (err) {
  if (err) {
    console.log('Error on creating test data, err:', err);
  } else {
    console.log('Done ----');
  }
  process.exit(0);
});

