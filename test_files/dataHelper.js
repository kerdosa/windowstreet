/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * Helper method to create test data.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var _ = require('lodash'),
  moment = require('moment'),
  addresses = require('./addresses');

// create test installer
exports.createInstaller = function(id) {
  var addr = addresses[_.random(0, 99)];
  return {
    companyName: 'Installer-'+id,
    contactName: 'Contact-'+id,
    address: addr.address,
    city: addr.city,
    state: addr.state,
    zipcode: addr.zipcode,
    country: 'USA',
    email: 'install'+id+'@sunshot.com',
    phoneNumber: ''+_.random(1112223333, 9998887777),
    numberOfEmployees: _.random(100, 10000),
    geographicalServiceArea: 'North America'
  };
};

// create a test endowment
exports.createEndowment = function(id, user) {
  return {
    name: 'Endowment-'+id,
    user: user._id
  };
};

// create a test borrower
exports.createBorrower = function(id, endowment) {
  var addr = addresses[_.random(0, 99)];
  return {
    name: 'Borrower-'+id,
    endowment: endowment._id,
    address: addr.address,
    city: addr.city,
    state: addr.state,
    zipcode: addr.zipcode,
    country: 'USA',
    email: 'borrow'+id+'@sunshot.com',
    phoneNumber: ''+_.random(1112223333, 9998887777),
  };
};

// create a test loan
exports.createLoan = function(id, borrower) {
  var year = _.random(2005, 2014), month = _.random(1,12), day = _.random(1,28);
  return {
    borrower: borrower._id,
    desiredLoanTerm: _.random(12, 60),
    householdAGI: _.random(50000, 900000),
    monthlyExpenses: _.random(100, 5000),
    loanTotal: _.random(10000, 10000000),
    automaticPayments: _.random(1) ? true : false,
    interestRate: _.random(1, 10),
    loanDate: new Date(year+'-'+month+'-'+day)
  };
};

// create a test loan payment
exports.createLoanPayment = function(id, loan) {
  var year = _.random(2005, 2014), month = _.random(1,12), day = _.random(1,28);
  return {
    loan: loan._id,
    paymentAmount: _.random(1000, 1000000),
    lateFees: _.random(0, 500),
    paymentDate: new Date(year+'-'+month+'-'+day)
  };
};

// create a test system
var index = 0;
exports.createSystem = function(id, installer, borrower) {
  var year = _.random(2005, 2014), month = _.random(1,12), day = _.random(1,28);
  var dateStr = year+'-'+month+'-'+day;
  var finishMoment = moment(dateStr, 'YYYY-M-D');
  var finishDate = new Date(finishMoment.format());
  var days = _.random(50, 300);
  finishMoment.subtract(days, 'days');
  var startDate = new Date(finishMoment.format());

  var addr;
  if (index >= 100) {
    addr = addresses[_.random(0, 99)];
  } else {
    addr = addresses[index];
  }
  if (!addr.lat || !addr.lng) {
    console.log('No lat or lng in addr:', addr);
  }
  index += 1;

  return {
    installer: installer._id,
    borrower: borrower._id,
    address: addr.address,
    city: addr.city,
    state: addr.state,
    zipcode: addr.zipcode,
    country: 'USA',
    lat: addr.lat,
    lng: addr.lng,
    size: _.random(1000.00, 99999.99),
    roofType: 'roofType'+id,
    projectedAnnualGeneration: _.random(10000, 999999),
    solarModuleManufacturer: 'Solar Systems-'+id,
    solarInverterManufacturer: 'GH Electronics-'+id,
    solarModuleRating: _.random(10),
    solarInverterType: 'Sunnyboy 6k',
    installationDate: startDate,
    finishDate: finishDate
  };
};

// create a test system performance
exports.createSystemPerformance = function(date, system) {
  return {
    system: system._id,
    dailyEnergy: _.random(1, 25),
    systemACP: _.random(1, 10),
    systemDCP: _.random(1, 10),
    systemDCV: _.random(10, 500),
    systemDCA: _.random(10, 100),
    date: new Date(date.format())
  };
};
