/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * Create test System Performance data to CSV file.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';


var csv = require('csv');
var fs = require('fs');

var beginning = (new Date()).getTime();
var startDate = process.argv[2] ? (new Date(process.argv[2])).getTime() : (new Date('1/1/2014')).getTime();
var data = [];

// write header
data.push(['dailyEnergy','systemACP','systemDCP','systemDCV','systemDCA','date']);

for (startDate; startDate < beginning;startDate += 900000) {
    var acp = Math.floor(Math.random() * 10) + 1;
    var dcp = Math.floor(Math.random() * 10) + 1;
    var dcv = Math.floor(Math.random() * 500) + 1;
    var dca = Math.floor(Math.random() * 100) + 1;
    var dailyEnergy = Math.floor(Math.random() * 25) + 1;

    data.push([
            dailyEnergy,
            acp,
            dcp,
            dcv,
            dca,
            startDate
        ]);
}

csv.stringify(data, function(err, data) {
    fs.writeFile('test.csv', data, function(err) {
        if (err) {
            console.log(err);
        }
    });
});

