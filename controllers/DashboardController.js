/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the controller for getting data for dashboard landing page.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


var dashboardService = require('../services/DashboardService');



/**
 * Get chart data for landing page.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function getLandingData(req, res, next) {
  dashboardService.getLandingData(req.user.id, next.wrap(function (landingData) {
    res.json(landingData);
  }));
}

/**
 * Get clean energy chart data.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function getCleanEnergyChartData(req, res, next) {
  dashboardService.getCleanEnergyChartData(req.user.id, req.query.previousData, req.query.nextData, false, next.wrap(function (cleanEnergy) {
    res.json(cleanEnergy);
  }));
}

/**
 * Get carbon impact chart data.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function getCarbonImpactChartData(req, res, next) {
  dashboardService.getCleanEnergyChartData(req.user.id, req.query.previousData, req.query.nextData, true, next.wrap(function (carbonImpact) {
    res.json(carbonImpact);
  }));
}

/**
 * Get money earned chart data.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function getMoneyEarnedChartData(req, res, next) {
  dashboardService.getMoneyEarnedChartData(req.user.id, next.wrap(function (loans) {
    res.json(loans);
  }));
}

/**
 * Get installed system chart data.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function getInstalledSystemChartData(req, res, next) {
  dashboardService.getInstalledSystemChartData(req.user.id, req.query.previousData, req.query.nextData, next.wrap(function (loans) {
    res.json(loans);
  }));
}

/**
 * Get install system map data.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function getInstalledSystemMapData(req, res, next) {
  dashboardService.getInstalledSystemMapData(req.user.id, next.wrap(function (systems) {
    res.json(systems);
  }));
}

module.exports = {
  getLandingData: getLandingData,
  getCleanEnergyChartData: getCleanEnergyChartData,
  getCarbonImpactChartData: getCarbonImpactChartData,
  getMoneyEarnedChartData: getMoneyEarnedChartData,
  getInstalledSystemChartData: getInstalledSystemChartData,
  getInstalledSystemMapData: getInstalledSystemMapData
};
