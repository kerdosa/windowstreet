/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is a controller for uploading SystemPerformance data.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

require('../helpers/function-utils');

var fs = require('fs'),
  mongoose = require('mongoose'),
  SystemPerformance = mongoose.model('SystemPerformance'),
  async = require('async'),
  csv2json = require('csv2json'),
  split = require('split'),
  errors = require('../errors');

// 1000 is max bulk insert size
var MONGO_BATCH_SIZE = 1000;


/**
 * Read CSV data file.
 * @param {Object} file the file path of CSV file
 * @param {String} systemId the System object ID
 * @param {Function} callback the callback function
 */
function _readCsvToJson(file, systemId, callback) {
  var dataArray = [];
  fs.createReadStream(file)
    .pipe(csv2json())
    .pipe(split())
    .on('data', function(line) {
      try {
        var data = JSON.parse(line);
        data.system = systemId;
        dataArray.push(data);
      } catch (e) {
      }
    })
    .on('end', function () {
      callback(null, dataArray);
    })
    .on('error', function (err) {
      callback(err);
    });
}


/**
 * Upload SystemPerformance CSV data file.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function uploadFile(req, res, next) {
  if (!req.files || !req.files.file) {
      return next(new errors.ValidationError('Please upload one csv file'));
  }
  var systemId = req.body.systemId;
  if (!systemId) {
    return next(new errors.ValidationError('systemId is missing'));
  }

  var file = req.files.file;

  async.waterfall([
    function (cb) {   // read csv file
      _readCsvToJson(file.path, systemId, cb);
    }, function (dataArray, cb) {   // split data into 1000 batch size
      var chunks = [];
      var i, tmp;
      for (i = 0; i < dataArray.length; i += MONGO_BATCH_SIZE) {
          tmp = dataArray.slice(i, i+MONGO_BATCH_SIZE);
          chunks.push(tmp);
      }
      cb(null, chunks);
    }, function (chunks, cb) {   // bulk insert to DB
      var created = 0;
      async.each(chunks, function (chunk, cb2) {
        SystemPerformance.collection.insert(chunk, function (err, data) {
          if (err) {
            cb2(err);
          } else if (data && data.result && data.result.ok === 1) {
            created += data.result.n;
            cb2();
          }
        });
      }, function (err) {
        cb(err, created);
      });
    }, function (created) {
      res.json({message: 'success', created: created});
    }
  ], next);
}

module.exports = {
  uploadFile: uploadFile
};