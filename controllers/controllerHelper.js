/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This module provides helper methods for creating express controllers.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

require('../helpers/function-utils');

var config = require('config'),
  mongoose = require('mongoose'),
  _ = require('lodash'),
  async = require('async'),
  errors = require('../errors'),
  validate = require('../helpers/validator').validate;


/**
 * Get reference model name of reference field.
 * @param {String} refName field name of reference
 * @returns {String} the model name of refName
 */
function _getReferenceModel(refName) {
  return refName.charAt(0).toUpperCase()+refName.slice(1);
}

/**
 * Check the value is MongoDB ObjectId or not.
 * @param {String} value any value to check
 * @returns {Boolean} true if it's ObjectId, false if not
 */
function _isObjectId(value) {
  if (value && /^[a-zA-Z0-9]{24}$/.test(value)) {
    return true;
  } else {
    return false;
  }
}

/**
 * This function checks references in given data.
 * A reference specifies the reference ID, referenced entity type and whether permission should be checked.
 * @param {Object} data the data
 * @param {Object} references array of references
 * @param {Funciton} callback callback function(err)
 */
function _checkReferences(data, references, callback) {
  if (!references) {
    references = [];
  }
  async.eachSeries(references, function (reference, cb) {
    var refKey = _.keys(reference)[0];
    var referenceId = data[refKey];
    if (!referenceId) {
      cb();
    } else {
      var refModel = _getReferenceModel(refKey);
      mongoose.model(refModel).findOne({_id : referenceId}, function (err, entity) {
        if (!entity) {
          cb(new errors.ValidationError('No ' + refModel + ' with ID ' + referenceId + ' is found.'));
        } else {
          cb();
        }
      });
    }
  }, callback);
}

/**
 * This function replaces the reference object with its id in the input object.
 * A reference specifies the reference ID, referenced entity type and whether permission should be checked.
 * @param {Object} data the input data
 * @param {Object} references array of references
 * @param {Funciton} callback callback function(err)
 */
function _unPopulate(data, references, callback) {
  if (!references) {
    references = [];
  }
  async.eachSeries(references, function (reference, cb) {
    var refKey = _.keys(reference)[0];
    var referenceObject = data[refKey];
    if (referenceObject && referenceObject.id) {
      // reference object is posted in create or update operation, 
      // replace the reference object with its id.
      data[refKey] = referenceObject.id;
    }
    cb();
  }, callback);
}

/**
 * This function gets an entity.
 * @param {String} entityType the entity type
 * @param {Object} references array of references from the entity to other entities
 * @param {String} id the entity ID
 * @param {Object} req the request
 * @param {Funciton} next the next function in the chain
 */
function findEntityById(entityType, references, id, req, next) {
  var error = validate(
    {id: id, _populate: Boolean(req.query._populate)},
    {id: 'objectId', _populate: 'boolean?'}
  );
  if (error) {
    return next(error);
  }

  var query = mongoose.model(entityType).findOne({_id : id});

  // populate references
  var populates = [];
  if (req.query._populate) {
    _.each(references, function (reference) {
      _.each(reference, function (value, key) {
        populates.push(key);
      });
    });
    _.each(populates, function (ref) {
      query.populate(ref);
    });
  }
  
  query.exec(function (err, entity) {
    if (err) {
      return next(err);
    }
    if (!entity) {
      next(new errors.NotFoundError('No ' + entityType + ' with ID ' + id + ' is found.'));
    } else {
      req.data = entity;
      next();
    }
  });
}

/**
 * This function gets an entity.
 * @param {String} entityType the entity type
 * @param {Object} req the request
 * @param {Object} res the response
 */
function getEntity(entityType, req, res) {
  res.json(req.data);
}

/**
 * This function creates an entity.
 * The entity data comes from req.body.
 * @param {String} entityType the entity type
 * @param {Object} references array of references
 * @param {Object} hooks the optional functions that's called after each CRUD operation
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Funciton} next the next function in the chain
 */
function createEntity(entityType, references, hooks, req, res, next) {
  async.waterfall([
    function (cb) { // un-populate
      _unPopulate(req.body, references, cb);
    },
    function (cb) { // check referenced entities and create
      _checkReferences(req.body, references, cb);
    },
    function (cb) { // create
      // exclude prohibited fields
      req.body = _.omit(req.body, '_id');
      mongoose.model(entityType).create(req.body, cb);
    },
    function (entity, cb) {
      if (hooks && hooks.create) {
        hooks.create(req, entity, function (err) {
          cb(err, entity);
        });
      } else {
        cb(null, entity);
      }
    }
  ], function (err, entity) {
    if (err) {
      return next(err);
    }
    if (references) {
      // return the reference populated object
      req.query._populate = true;
      findEntityById(entityType, references, entity.id, req, next.wrap(function () {
        res.json(req.data);
      }));
    } else {
      res.json(entity);
    }
  });
}

/**
 * This function updates an entity.
 * The entity data comes from req.body.
 * @param {String} entityType the entity type
 * @param {Object} references array of references
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Funciton} next the next function in the chain
 */
function updateEntity(entityType, references, req, res, next) {
  async.waterfall([
    function (cb) { // un-populate
      _unPopulate(req.body, references, cb);
    },
    function (cb) {
    // exclude prohibited fields
    req.body = _.omit(req.body, '_id');
    req.data = _.extend(req.data, req.body);
      _checkReferences(req.data, references, cb);
    },
    function (cb) {
      req.data.save(cb);
    }
  ], function (err, entity) {
    if (err) {
      return next(err);
    }
    if (references) {
      // return the reference populated object
      req.query._populate = true;
      findEntityById(entityType, references, entity.id, req, next.wrap(function () {
        res.json(req.data);
      }));
    } else {
      res.json(entity);
    }
  });

}

/**
 * This function deletes an entity.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Funciton} next the next function in the chain
 */
function deleteEntity(req, res, next) {
  req.data.remove(next.wrap(function () {
    res.json(req.data);
  }));
}

/**
 * This function retrieves all entities, search filters are supported.
 * @param {String} entityType the entity type
 * @param {String} nameField the field representing name in the model, e.g. 'name'
 * @param {Object} references array of references from the entity to other entities
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Funciton} next the next function in the chain
 */
function getAllEntities (entityType, nameField, references, req, res, next) {
  var error = validate(
    {_pageSize: req.query._pageSize, _pageNumber: req.query._pageNumber, _orderBy: req.query._orderBy, 
      _orderDir: req.query._orderDir, _populate: Boolean(req.query._populate)},
    {_pageSize: 'number?', _pageNumber: 'number?', _orderBy: 'string?', _orderDir: 'string?', _populate: 'boolean?'}
  );
  if (error) {
    return next(error);
  }

  var paginateOptions = {}, filters = {}, refFilters = [], refSortBy;
  // extract page number & page size
  var pageNumber = req.query._pageNumber ? Number(req.query._pageNumber) : 1; // page starts from 1
  var pageSize = req.query._pageSize ? Number(req.query._pageSize) : config.PAGE_SIZE;
  delete req._pageNumber;
  delete req._pageSize;
  if (pageSize === -1) {  // get all data witih id and name only
    pageNumber = 1;
    pageSize = Number.MAX_SAFE_INTEGER;
    paginateOptions.columns = '_id';
    if (nameField) {
      paginateOptions.columns += ' '+nameField;
    }
  } else {
    var model = mongoose.model(entityType);

    // building sortBy
    var sortBy = {};
    if (req.query._orderBy) {
      var orderBy = req.query._orderBy;
      if (!model.schema.paths[orderBy]) {
        return next(new errors.ValidationError('orderBy parameter ' + orderBy + ' is not a field of '+entityType));
      }
      
      var orderDir = req.query._orderDir && (req.query._orderDir.toLowerCase() === 'desc') ? -1 : 1;  // asc is default
      if (references && _.find(references, orderBy)) {  // order by reference name field
        var reference = _.find(references, orderBy);
        refSortBy = {
          key: orderBy,
          field: reference[orderBy],
          dir: orderDir
        };
      } else {  // regular field
        sortBy[orderBy] = orderDir;
        paginateOptions.sortBy = sortBy;
      }
      delete req.query._orderBy;
      delete req.query._orderDir;
    }

    // populate references
    if (req.query._populate) {
      paginateOptions.populate = [];
      _.each(references, function (reference) {
        _.each(reference, function (value, key) {
          var populate = {
            path: key,
            select: '_id '+value
          };
          paginateOptions.populate.push(populate);
        });
      });
      delete req.query._populate;
    } else {  
      refSortBy = null;   // disable reference sorting
    }

    // build field search filter
    _.each(req.query, function (value, key) {
      // ignore all other parameters if it's not path or no value
      var path = model.schema.paths[key];
      if (path && value) {
        if (references && _.find(references, key)) {    // if filtering is in reference
          // if the value is ObjectId, then it's reference id filtering in the current model
          if (_isObjectId(value)) {
            filters[key] = value;
          } else {
            var reference = _.find(references, key);
            var refField = reference[key];
            var refFilter = {
              model: _getReferenceModel(key),
              ref: key,
              filter: {}
            };
            refFilter.filter[refField] = new RegExp(value, 'i');
            refFilters.push(refFilter);
          }
        } else if (path.instance === 'String') {
          filters[key] = new RegExp(value, 'i');
        } else if ((path.instance === 'Number') || (path.instance === 'Date')) {   // range filter for Number and Date type
          value = value.replace(/ /g, '');
          var parts = value.split(',');   //range filter values: field=min,max
          var minMaxFilter = {};
          if (parts[0]) {
            minMaxFilter.$gte = path.instance === 'Number' ? Number(parts[0]) : new Date(parts[0]);
          }
          if (parts[1]) {
            minMaxFilter.$lte = path.instance === 'Number' ? Number(parts[1]) : new Date(parts[1]);
          }
          filters[key] = minMaxFilter;
        }
      }
    });
  }
  
  async.waterfall([
    function (cb) {   // reference filtering
      async.each(refFilters, function (refFilter, cb2) {
        mongoose.model(refFilter.model).find(refFilter.filter, '_id', function (err, ids) {
          filters[refFilter.ref] = {$in: ids};
          cb2(err);
        });
      }, cb);
    },
    function (cb) {   // execute the paginated query
      mongoose.model(entityType).paginate(filters, pageNumber, pageSize,
        function (err, pageCount, paginatedResults, itemCount) {
          var data = {
            total : itemCount,
            totalPages : pageCount,
            page: pageNumber,
            results : paginatedResults
          };
          cb(err, data);
        },
        paginateOptions
      );
    },
    function (data, cb) {   // reference sorting
      if (refSortBy) {
        var sorted = _.sortBy(data.results, function (entity) {
          if (entity[refSortBy.key] && entity[refSortBy.key][refSortBy.field]) {
            return entity[refSortBy.key][refSortBy.field];
          } else {
            return '';
          }
        });
        if (refSortBy.dir === -1) {    // descending
          sorted.reverse();
        }
        data.results = sorted;
        cb(null, data);
      } else {
        cb(null, data);
      }
    }
  ], next.wrap(function (data) {
    res.json(data);
  }));

}

/**
 * This method builds controller for an entity type.
 * @param {String} entityType the entity type
 * @param {String} nameField the field representing name in the model, e.g. 'name'
 * @param {Object} references array of references from the entity to other entities.
 *    e.g. {borrower: '_id name'}, key is the field and value is the fields to populate
 * @param {Object} hooks the optional functions that's called after each CRUD operation
 * @returns {Object} the controller
 */
exports.buildEntityController = function(entityType, nameField, references, hooks) {
  var controller = {};

  /**
   * Find an entity by id.
   * @param {Object} req the request
   * @param {Object} res the response
   * @param {Funciton} next the next function in the chain
   * @param {String} id the entity ID
   */
  controller.findById = function(req, res, next, id) {
    findEntityById(entityType, references, id, req, next);
  };

  /**
   * Get an entity.
   * @param {Object} req the request
   * @param {Object} res the response
   * @param {Funciton} next the next function in the chain
   */
  controller.get = function(req, res, next) {
    getEntity(entityType, req, res, next);
  };

  /**
   * Create an entity.
   * @param {Object} req the request
   * @param {Object} res the response
   * @param {Funciton} next the next function in the chain
   */
  controller.create = function(req, res, next) {
    createEntity(entityType, references, hooks, req, res, next);
  };

  /**
   * Update an entity.
   * @param {Object} req the request
   * @param {Object} res the response
   * @param {Funciton} next the next function in the chain
   */
  controller.update = function(req, res, next) {
    updateEntity(entityType, references, req, res, next);
  };

  /**
   * Retrieve all entities, field filtering, paging and sorting are supported.
   * @param {Object} req the request
   * @param {Object} res the response
   * @param {Funciton} next the next function in the chain
   */
  controller.all = function(req, res, next) {
    getAllEntities(entityType, nameField, references, req, res, next);
  };

  /**
   * Delete an entity.
   * @param {Object} req the request
   * @param {Object} res the response
   * @param {Funciton} next the next function in the chain
   */
  controller.delete = function(req, res, next) {
    deleteEntity(req, res, next);
  };

  return controller;
};