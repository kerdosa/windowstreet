/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the controller for user and authentication.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


var async = require('async'),
  _ = require('lodash'),
  errors = require('../errors'),
  helper = require('../helpers/helper'),
  validate = require('../helpers/validator').validate,
  userService = require('../services/UserService');


/**
 * Handle reset password request when user clicks reset password link.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function forgotPassword(req, res, next) {
  var error = validate({email: req.query.email}, {email: 'email'});
  if (error) {
    return next(error);
  }

  async.waterfall([
    function (cb) {  // find a user
      userService.findByEmail(req.query.email, cb);
    },
    function (foundUser, cb) {
      if (!foundUser) {
        return cb(new errors.NotFoundError('User not found'));
      }
      userService.sendEmail(req, foundUser, cb);
    }
  ], next.wrap (function () {
    res.json({message: 'ok'});
  }));
}

/**
 * Handle reset password request when user clicks reset password link.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function resetPasswordLink(req, res, next) {
  var error = validate(
    {email: req.query.email, code: req.query.code}, 
    {email: 'email', code: String}
  );
  if (error) {
    return next(error);
  }

  var user, tmpPassword;
  async.waterfall([
    function (cb) {  // find a user
      userService.findByEmail(req.query.email, cb);
    },
    function (foundUser, cb) {
      if (!foundUser) {
        return cb(new errors.NotFoundError('User not found'));
      }
      user = foundUser;
      helper.hashPassword(req.query.code, cb);
    },
    function (hash, cb) {   // check code
      if (hash !== user.passwordHash) {
        return cb(new errors.ValidationError('The code is invalid'));
      } else {
        // generate new code
        helper.randomPassword(cb);
      }
    },
    function (password, cb) {
      tmpPassword = password;
      helper.hashPassword(password, cb);
    },
    function (hash, cb) {
      user.passwordHash = hash;
      user.save(cb);
    }
  ], next.wrap (function () {
    // send reset password form
    res.render('reset', {
      code: tmpPassword, 
      email: user.email
    });
  }));
}

/**
 * Reset user password from reset password view.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function resetPassword(req, res, next) {
  var error = validate(
    {code: req.body.code, email: req.body.email, password: req.body.password, confirmPassword: req.body.confirmPassword},
    {code: String, email: 'email', password: String, confirmPassword: String}
  );
  if (error) {
    return next(error);
  }

  var password = req.body.password;
  var confirmPassword = req.body.confirmPassword;
  if (password !== confirmPassword) {
    return next(new errors.ValidationError('Password does not mathc with confirm password'));
  }

  var user;
  async.waterfall([
    function (cb) {  // find user by email
      userService.findByEmail(req.body.email, cb);
    },
    function (foundUser, cb) {   // check code
      if (!foundUser) {
        return cb(new errors.NotFoundError('User not found'));
      }
      user = foundUser;
      helper.hashPassword(password, cb);
    },
    function (hash, cb) {
      user.passwordHash = hash;
      user.save(cb);
    }
  ], next.wrap (function () {
    // redirect to /
    res.redirect('/');
  }));
}

/**
 * Login user.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function login(req, res, next) {
  async.waterfall([
    function (cb) {
      userService.authenticate(req.body.email, req.body.password, cb);
    },
    function (user, cb) {
      if (!user) {
        return cb(new errors.NotFoundError('Invalid username or password'));
      }
      req.logIn(user, cb);
    }
  ], next.wrap (function () {
    res.json(req.user);
  }));
}

/**
 * Logout user.
 * @param {Object} req the request
 * @param {Object} res the response
 */
function logout(req, res) {
  req.logout();
  res.json({message: 'user logged out'});
}

/**
 * Get current logged in user.
 * @param {Object} req the request
 * @param {Object} res the response
 * @param {Function} next the next middleware
 */
function getUserMe(req, res, next) {
  if (req.user && req.user.id) {
    res.json(_.pick(req.user, ['id', 'email', 'role']));
  } else {
    next(new errors.NotFoundError('Logged in user is not found'));
  }
}

module.exports = {
  forgotPassword: forgotPassword,
  resetPasswordLink: resetPasswordLink,
  resetPassword: resetPassword,
  login: login,
  logout: logout,
  getUserMe: getUserMe
};
