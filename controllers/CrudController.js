/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the controller for building CRUD routes and operations for every model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var middlewares = require('../helpers/middlewares'),
  userService = require('../services/UserService');


var user = require('./controllerHelper').buildEntityController('User', 'email', null, {create: userService.sendEmail});

var endowment = require('./controllerHelper').buildEntityController('Endowment', 'name',
  [{user: 'email'}]
);

var borrower = require('./controllerHelper').buildEntityController('Borrower', 'name',
  [{endowment: 'name'}]
);

var loan = require('./controllerHelper').buildEntityController('Loan', null,
  [{borrower: 'name'}]
);

var loanPayment = require('./controllerHelper').buildEntityController('LoanPayment', null, null);

var installer = require('./controllerHelper').buildEntityController('Installer', 'companyName', null);

var system = require('./controllerHelper').buildEntityController('System', null,
  [{installer: 'companyName'}, {borrower: 'name'}]
);

var systemPerformance = require('./controllerHelper').buildEntityController('SystemPerformance', null, null);

/**
 * Utility function to setup routes for an entity controller
 * @param app the app object
 * @param controller the controller
 * @param baseUrl the base URL for the controller
 * @param idParameter the name of the "id" parameter
 */
function _setupRoutes(app, controller, baseUrl, idParameter) {
  app.route(baseUrl)
    .get(middlewares.requireAdmin, controller.all)
    .post(middlewares.requireAdmin, controller.create);
  app.route(baseUrl + '/:' + idParameter)
    .get(middlewares.requireAdmin, controller.get)
    .put(middlewares.requireAdmin, controller.update)
    .delete(middlewares.requireAdmin, controller.delete);
  app.param(idParameter, controller.findById);
}

/* jshint -W098 */
module.exports = function(app) {

  // User
  _setupRoutes(app, user, '/users', 'userId');

  // Endowment
  _setupRoutes(app, endowment, '/endowments', 'endowmentId');

  // Borrower
  _setupRoutes(app, borrower, '/borrowers', 'borrowerId');

  // Loan
  _setupRoutes(app, loan, '/loans', 'loanId');

  // LoanPayment
  _setupRoutes(app, loanPayment, '/loanPayments', 'loanPaymentId');

  // Installer
  _setupRoutes(app, installer, '/installers', 'installerId');

  // System
  _setupRoutes(app, system, '/systems', 'systemId');

  // SystemPerformance
  _setupRoutes(app, systemPerformance, '/systemPerformances', 'systemPerformanceId');

};