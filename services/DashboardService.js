/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the service for getting data for dashboard landing page.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


var async = require('async'),
  _ = require('lodash'),
  errors = require('../errors'),
  validate = require('../helpers/validator').validate,
  geolib = require('geolib'),
  mongoose = require('mongoose'),
  ObjectId = mongoose.Types.ObjectId,
  Endowment = mongoose.model('Endowment'),
  Borrower = mongoose.model('Borrower'),
  SystemModel = mongoose.model('System'),
  SystemPerformance = mongoose.model('SystemPerformance'),
  Loan = mongoose.model('Loan');

// carbon impact rate
var CARBON_IMPACT_RATE = 0.45;

/**
 * Get endowment, borrower and system ids of user.
 * @param {Object} userId the user id of endowment
 * @param {Function} callback the callback function
 */
function _getEntityIdsByUser(userId, includeSystem, callback) {
  var entityIds = {};
  async.waterfall([
    function (cb) {
      // the relationship of user and endowment is 1-to-1
      Endowment.findOne({user: userId}, {_id: 1}, cb);
    },
    function (data, cb) {
      if (!data) {
        return cb(new errors.NotFoundError('User does not have endowment'));
      } else {
        entityIds.endowmentId = data._id;
        Borrower.find({endowment: entityIds.endowmentId}, {_id: 1}, cb);
      }
    },
    function (data, cb) {
      entityIds.borrowerIds = _.pluck(data, '_id');
      if (includeSystem) {
        SystemModel.find({borrower: {$in: entityIds.borrowerIds}}, {_id: 1}, cb);
      } else {
        cb(null);
      }
    }
  ], function (err, data) {
    entityIds.systemIds = _.pluck(data, '_id');
    callback(err, entityIds);
  });
}

/**
 * Get chart data for landing page.
 * @param {Object} userId the user id of endowment
 * @param {Function} callback the callback function
 */
function getLandingData(userId, callback) {
  var error = validate({userId: userId}, {userId: 'objectId'});
  if (error) {
    return callback(error);
  }

  var landingData = {};
  async.waterfall([
    function (cb) {
      _getEntityIdsByUser(userId, true, cb);
    },
    function (entityIds, cb) { // start & end year
      var systemObjectIds = _.map(entityIds.systemIds, function (systemId) {
        return new ObjectId(systemId);
      });
      SystemPerformance.aggregate()
        .match({system: {$in: systemObjectIds}})
        .sort({'date': 1})
        .group({_id: null, first: {$first: '$date'}, last: {$last: '$date'}, totalCleanEnergy: {$sum: '$dailyEnergy'}})
        .exec(function (err, data) {
          if (data && data.length > 0) {
            landingData.totalCleanEnergy = data[0].totalCleanEnergy;
            landingData.totalCarbonImpact = Math.round(landingData.totalCleanEnergy * CARBON_IMPACT_RATE);
            landingData.startYear = data[0].first.getFullYear();
            landingData.endYear = data[0].last.getFullYear();
          }
          cb(err, entityIds);
        });
    },
    function (entityIds, cb) {  // sum of loanTotal and loanTotal*interrestRate
      var borrowerObjectIds = _.map(entityIds.borrowerIds, function (borrowerId) {
        return new ObjectId(borrowerId);
      });
      Loan.aggregate()
        .match({borrower: {$in: borrowerObjectIds}})
        .project({loanTotal: 1, interest: {$divide: ['$interestRate', 100]}})
        .group({_id: null, totalMoneyInvested: {$sum: '$loanTotal'}, totalMoneyEarned: {$sum: {$multiply: ['$loanTotal', '$interest']}}})
        .exec(function (err, data) {
          if (data && data.length > 0) {
            landingData.totalMoneyInvested = data[0].totalMoneyInvested;
            landingData.totalMoneyEarned = Math.round(data[0].totalMoneyEarned);
            var returnRate = (landingData.totalMoneyEarned / landingData.totalMoneyInvested).toFixed(2);
            landingData.returnRate = Math.round(Number(returnRate) * 100); // unit is percent
          }
          cb(err, borrowerObjectIds);
        });
    },
    function (borrowerObjectIds, cb) {  // total installed system
      SystemModel.aggregate()
        .match({borrower: {$in: borrowerObjectIds}})
        .group({_id: null, totalInstalledSystem: {$sum: 1}})
        .exec(function (err, data) {
          if (data && data.length > 0) {
            landingData.totalInstalledSystem = data[0].totalInstalledSystem;
          }
          cb(err, landingData);
        });
    }
  ], callback);
  
}

/**
 * Get lower/upper date bound in UTC.
 * @param {String} previousData the upper bound of year
 * @param {String} nextData the lower bound of year
 * @param {Function} callback the callback function
 */
function _getDateBoundInUTC(previousData, nextData) {
  var endYear = new Date().getFullYear(); // current year
  var startYear = endYear - 3;
  if (nextData) {
    startYear = Number(nextData) + 1;
    endYear = startYear + 3;
  } else if (previousData) {
    endYear = Number(previousData) - 1;
    startYear = endYear - 3;
  }

  var startDate = new Date(startYear, 0, 1);
  startDate.setUTCHours(0, 0, 0);
  var endDate = new Date(endYear, 11, 31);
  endDate.setUTCHours(0, 0, 0);
  return {startDate: startDate, endDate: endDate};
}

/**
 * Get total clean energy chart data.
 * @param {Object} userId the user id of endowment
 * @param {String} previousData the upper bound of year
 *   When previousData is 2012, 4 years data of 2008-2011 are returned.
 * @param {String} nextData the lower bound of year
 *   When nextData is 2012, 4 years data of 2013-2016 are returned
 * @param {Boolean} carbonImpact return carbon impact when it's true
 * @param {Function} callback the callback function
 */
function getCleanEnergyChartData(userId, previousData, nextData, carbonImpact, callback) {
  var error = validate(
    {userId: userId, previousData: previousData, nextData: nextData, carbonImpact: carbonImpact}, 
    {userId: 'objectId', previousData: 'year?', nextData: 'year?', carbonImpact: Boolean}
  );
  if (error) {
    return callback(error);
  }
  if (nextData && previousData) {
    return callback(new errors.ValidationError('Only one of previousData or nextData is required'));
  }

  var bound = _getDateBoundInUTC(previousData, nextData);

  // get monthly aggregated dailyEnergy of 4 years 
  async.waterfall([
    function (cb) {
      _getEntityIdsByUser(userId, true, cb);
    },
    function (entityIds, cb) {
      var systemObjectIds = _.map(entityIds.systemIds, function (systemId) {
        return new ObjectId(systemId);
      });
      SystemPerformance.aggregate()
        .match({system: {$in: systemObjectIds}, date: {$gte: bound.startDate, $lte: bound.endDate}})
        .group({_id: {year: {$year: '$date'}, month: {$month: '$date'}}, total: {$sum: '$dailyEnergy'}})
        .sort({'_id.year': 1, '_id.month': 1})
        .exec(function (err, data) {
          var monthlyAggregatedData = _.map(data, function(monthlyData) {
            var kv = {};
            var key = monthlyData._id.month+'/'+monthlyData._id.year;
            if (carbonImpact) {
              kv[key] = Math.round(monthlyData.total * CARBON_IMPACT_RATE);
            } else {
              kv[key] = monthlyData.total;
            }
            return kv;
          });
          cb(err, monthlyAggregatedData);
        });
    }
  ], callback);
}

/**
 * Get total earned money chart data.
 * @param {Object} userId the user id of endowment
 * @param {Function} callback the callback function
 */
function getMoneyEarnedChartData(userId, callback) {
  // latest 28 loanTotal
  async.waterfall([
    function (cb) {
      _getEntityIdsByUser(userId, false, cb);
    },
    function (entityIds, cb) {
      Loan.find({borrower: {$in: entityIds.borrowerIds}})
        .sort({'loanDate': -1})
        .limit(28)
        .select('-_id loanTotal')
        .exec(function (err, data) {
          cb(err, data);
        });
    }
  ], callback);
}

/**
 * Get installed system chart data.
 * @param {Object} userId the user id of endowment
 * @param {String} previousData the upper bound of year
 *   When previousData is 2012, 4 years data of 2008-2011 are returned.
 * @param {String} nextData the lower bound of year
 *   When nextData is 2012, 4 years data of 2013-2016 are returned
 * @param {Function} callback the callback function
 */
function getInstalledSystemChartData(userId, previousData, nextData, callback) {
  var error = validate(
    {userId: userId, previousData: previousData, nextData: nextData},
    {userId: 'objectId', previousData: 'year?', nextData: 'year?'}
  );
  if (error) {
    return callback(error);
  }
  if (nextData && previousData) {
    return callback(new errors.ValidationError('Only one of nextData or previousData is required'));
  }

  var bound = _getDateBoundInUTC(previousData, nextData);

  // aggregate the number of installed system into monthly for 4 years
  async.waterfall([
    function (cb) {
      _getEntityIdsByUser(userId, false, cb);
    },
    function (entityIds, cb) {
      var borrowerObjectIds = _.map(entityIds.borrowerIds, function (borrowerId) {
        return new ObjectId(borrowerId);
      });
      SystemModel.aggregate()
        .match({borrower: {$in: borrowerObjectIds}, installationDate: {$gte: bound.startDate, $lte: bound.endDate}})
        .group({_id: {year: {$year: '$installationDate'}, month: {$month: '$installationDate'}}, count: {$sum: 1}})
        .sort({'_id.year': 1, '_id.month': 1})
        .exec(function (err, data) {
          var monthlyAggregatedData = _.map(data, function(monthlyData) {
            var kv = {};
            var key = monthlyData._id.month+'/'+monthlyData._id.year;
            kv[key] = monthlyData.count;
            return kv;
          });
          cb(err, monthlyAggregatedData);
        });
    }
  ], callback);
}

/**
 * Get installed system map data.
 * @param {Object} userId the user id of endowment
 * @param {Function} callback the callback function
 */
function getInstalledSystemMapData(userId, callback) {
  var error = validate({userId: userId}, {userId: 'objectId'});
  if (error) {
    return callback(error);
  }

  async.waterfall([
    function (cb) {
      _getEntityIdsByUser(userId, false, cb);
    },
    function (entityIds, cb) {
      var borrowerObjectIds = _.map(entityIds.borrowerIds, function (borrowerId) {
        return new ObjectId(borrowerId);
      });
      SystemModel.find({borrower: {$in: borrowerObjectIds}})
        .populate('installer', 'companyName')
        .populate('borrower', 'name')
        .exec(cb);
    },
    function (systemModels, cb) {
      var systems = [];
      async.each(systemModels, function (systemModel, cb2) {
        var system = systemModel.toJSON();
        if (system.borrower) {
          system.borrowerName = system.borrower.name;
          delete system.borrower;
        }
        if (system.installer) {
          system.installerCompanyName = system.installer.companyName;
          delete system.installer;
        }
        systems.push(system);
        cb2();
      }, function (err) {
        cb(err, systems);
      });
    },
    function (systems, cb) {  // find a center
      var locations = _.map(systems, function (system) {
        return {latitude: system.lat, longitude: system.lng};
      });
      var center = geolib.getCenter(locations);
      cb(null, {center: center, systems: systems});
    }
  ], callback);
}

module.exports = {
  getLandingData: getLandingData,
  getCleanEnergyChartData: getCleanEnergyChartData,
  getMoneyEarnedChartData: getMoneyEarnedChartData,
  getInstalledSystemChartData: getInstalledSystemChartData,
  getInstalledSystemMapData: getInstalledSystemMapData
};