/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the service for User model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


var config = require('config'),
  async = require('async'),
  mongoose = require('mongoose'),
  validate = require('../helpers/validator').validate,
  helper = require('../helpers/helper'),
  User = mongoose.model('User');

/**
 * Authenticate with email and password.
 * @param {String} email the user email
 * @param {String} password the password
 * @param {function} callback the callback function
 */
function authenticate(email, password, callback) {
  var error = validate(
    {email: email, password: password},
    {email: String, password: String}
  );
  if (error) {
    return callback(error);
  }
  User.findOne({email: email, passwordHash: helper.hashPasswordSync(password)}, function (err, user) {
    callback(err, user);
  });
}

/**
 * Find a user by id
 * @param {String} id the user id
 * @param {function} callback the callback function
 */
function get(id, callback) {
  User.findOne({_id: id}, callback);
}

/**
 * Find a user by email.
 * @param {String} email the user email
 * @param {function} callback the callback function
 */
function findByEmail(email, callback) {
  User.findOne({email: email}, callback);
}

/**
 * Send an email to user with reset password link to notify user account is created.
 * @param {Object} req the the request
 * @param {Object} user the user object
 * @param {function} callback the callback function
 */
function sendEmail(req, user, callback) {
  var tmpPassword;
  async.waterfall([
    function (cb) {
      helper.randomPassword(cb);
    }, function (password, cb) {
      tmpPassword = password;

      helper.hashPassword(password, cb);
    }, function (hash, cb) {
      user.passwordHash = hash;
      user.save(cb);
    }, function (result, count, cb) {
      var host = 'http://' + req.headers.host;
      // send reset password link and dahboard URL
      var values = {
        resetPasswordLink: host +'/resetPasswordLink?code='+tmpPassword+'&email='+result.email,
        dashboardLink: 'http://' + req.headers.host + '/'
      };
      helper.sendEmail(config.FROM_EMAIL, user.email, 'RESET_PASSWORD', values, cb);
    }
  ], function (err) {
    callback(err, null);
  });
}

module.exports = {
  authenticate: authenticate,
  get: get,
  findByEmail: findByEmail,
  sendEmail: sendEmail
};
