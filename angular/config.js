/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This contains the configuration of angular app.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

angular.module('sunshotApp')

// configure toastr
.config(function(toastrConfig) {  
  angular.extend(toastrConfig, {
    closeButton: true,
    positionClass: 'toast-bottom-right'
  });
})
// user roles
.constant('USER_ROLE', {
  admin: 'Admin',
  endowment: 'Endowment'
})
// default ui-grid grid options
.constant('GRID_OPTIONS', {
    enableSorting: true,
    enableColumnMenus: false,
    enableFiltering: true,
    paginationPageSizes: [10, 25, 50],
    paginationPageSize: 10,
    enableColumnResizing: true
})
// When external filtering is used in ui-grid and fliter value changes, delay sending the query to server
.constant('FILTER_QUERY_DELAY', 300); // unit is millisecond