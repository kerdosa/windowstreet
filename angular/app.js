/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the angularjs main app.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


angular.module('sunshotApp', ['ngResource', 'ui.bootstrap', 'ui.router', 'toastr', 'angularFileUpload',
      'ui.grid', 'ui.grid.pagination', 'ui.grid.resizeColumns', 'ui.grid.selection'])

.config(['$stateProvider', '$urlRouterProvider','$locationProvider', '$compileProvider', 
  function($stateProvider, $urlRouterProvider, $locationProvider, $compileProvider) {

  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);
  $urlRouterProvider.otherwise('/');

  function checkUserLogin($rootScope, $state, $q, $location, $timeout, UserService) {
    var deferred = $q.defer();
    UserService.getUserMe().then(function (user) {
      $rootScope.user = user;
      $timeout(deferred.resolve);
    }, function () {
      $timeout(deferred.reject);
      $location.path('/login');
    });
    return deferred.promise;
  }

  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'partials/login.html',
      controller: 'LoginController'
    })
    .state('portal', {
      abstract: true,
      url: '/portal',
      template: '<div data-ui-view></div>',
      resolve: {
        checkUserLogin: checkUserLogin
      }
    })
    .state('portal.landing', {
      url: '^/',
      templateUrl: 'partials/landing.html',
      controller: 'LandingController',
      resolve: {
        landingData: ['DashboardService', function (DashboardService) {
          return DashboardService.landing();
        }]
      }
    })
    .state('portal.admin', {
      abstract: true,
      url: '^/admin',
      templateUrl: 'partials/admin.html',
      controller: 'AdminController',
      resolve: {
        pagedUsers: ['User', function (User) {
          return User.query({_pageSize: -1}).$promise;
        }]
      }
    })
      .state('portal.admin.user', {
        url: '/user',
        templateUrl: 'partials/entityList.html',
        controller: 'UserController',
        resolve: {
          pagedUsers: ['User', function (User) {
            return User.query().$promise;
          }]
        },
        data: {
          entityName: 'User'
        }
      })
      .state('portal.admin.endowment', {
        url: '/endowment',
        templateUrl: 'partials/entityList.html',
        controller: 'EndowmentController',
        resolve: {
          pagedEndowments: ['Endowment', function (Endowment) {
            return Endowment.query().$promise;
          }]
        },
        data: {
          entityName: 'Endowment'
        }
      })
      .state('portal.admin.installer', {
        url: '/installer',
        templateUrl: 'partials/entityList.html',
        controller: 'InstallerController',
        resolve: {
          pagedInstallers: ['Installer', function (Installer) {
            return Installer.query().$promise;
          }]
        },
        data: {
          entityName: 'Installer'
        }
      })
      .state('portal.admin.borrower', {
        url: '/borrower?endowmentId',
        templateUrl: 'partials/entityList.html',
        controller: 'BorrowerController',
          resolve: {
            pagedBorrowers: ['$stateParams', 'Borrower', function ($stateParams, Borrower) {
              var params = {};
              if ($stateParams.endowmentId) {
                params.endowment = $stateParams.endowmentId;
              }
              return Borrower.query(params).$promise;
            }],
            pagedEndowments: ['Endowment', function (Endowment) {
              return Endowment.query({_pageSize: -1}).$promise;
            }]
          },
          data: {
            entityName: 'Borrower'
          }
      })
      .state('portal.admin.loan', {
        url: '/loan/:borrowerId',
        templateUrl: 'partials/loans.html',
        controller: 'LoanController',
        resolve: {
          borrower: ['$stateParams', 'Borrower', function ($stateParams, Borrower) {
            return Borrower.get({id: $stateParams.borrowerId, _populate: true}).$promise;
          }],
          pagedLoans: ['$stateParams', 'Loan', function ($stateParams, Loan) {
            return Loan.query({borrower: $stateParams.borrowerId}).$promise;
          }]
        },
        data: {
          entityName: 'Loan'
        }
      })
      .state('portal.admin.loanPayment', {
        url: '/loanPayment/:loanId',
        templateUrl: 'partials/loanPayments.html',
        controller: 'LoanPaymentController',
        resolve: {
          loan: ['$stateParams', 'Loan', function ($stateParams, Loan) {
            return Loan.get({id: $stateParams.loanId}).$promise;
          }],
          pagedLoanPayments: ['$stateParams', 'LoanPayment', function ($stateParams, LoanPayment) {
            return LoanPayment.query({loan: $stateParams.loanId}).$promise;
          }]
        },
        data: {
          entityName: 'LoanPayment'
        }
      })
      .state('portal.admin.system', {
        url: '/system?borrowerId',
        templateUrl: 'partials/entityList.html',
        controller: 'SystemController',
        resolve: {
          pagedBorrowers: ['Borrower', function (Borrower) {
            // only get id and name
            return Borrower.query({_pageSize: -1}).$promise;
          }],
          pagedInstallers: ['Installer', function (Installer) {
            // only get id and companyName
            return Installer.query({_pageSize: -1}).$promise;
          }],
          pagedSystems: ['$stateParams', 'System', function ($stateParams, System) {
            var params = {};
            if ($stateParams.borrowerId) {
              params.borrower = $stateParams.borrowerId;
            }
            return System.query(params).$promise;
          }],
        },
        data: {
          entityName: 'System'
        }
      })
      .state('portal.admin.systemPerformance', {
        url: '/systemPerformance/:systemId',
        templateUrl: 'partials/systemPerformances.html',
        controller: 'SystemPerformanceController',
        resolve: {
          system: ['$stateParams', 'System', function ($stateParams, System) {
            return System.get({id: $stateParams.systemId}).$promise;
          }],
          pagedPerformances: ['$stateParams', 'GRID_OPTIONS', 'SystemPerformance', function ($stateParams, GRID_OPTIONS, SystemPerformance) {
            if (!$stateParams.pageSize) {
              $stateParams.pageSize = GRID_OPTIONS.paginationPageSize;
            }
            return SystemPerformance.query({system: $stateParams.systemId, _pageSize: $stateParams.pageSize, _pageNumber: $stateParams.pageNumber}).$promise;
          }]
        },
        data: {
          entityName: 'SystemPerformance'
        }
      });
}])
.run(['$rootScope', '$log', '$state', '$location', '$timeout', '$window', 'UserService', 'GRID_OPTIONS', 'USER_ROLE', 'FILTER_QUERY_DELAY',
  function($rootScope, $log, $state, $location, $timeout, $window, UserService, GRID_OPTIONS, USER_ROLE, FILTER_QUERY_DELAY) {
    if (window.USER && window.USER.id) {//jshint ignore:line
      $rootScope.user = window.USER;    //jshint ignore:line
    }
    $rootScope.GRID_OPTIONS = GRID_OPTIONS;
    $rootScope.FILTER_QUERY_DELAY = FILTER_QUERY_DELAY;

    $rootScope.$on('$stateChangeStart', function (event) {
      $rootScope.showGrid = false;
      // check if the user has a role
      if ($rootScope.user) {
        var path = $location.path();
        if ((!path || path === '/') && $rootScope.user.role !== USER_ROLE.endowment) {
          $location.path('/admin/user');
        } else if (path.indexOf('/admin') === 0 && $rootScope.user.role !== USER_ROLE.admin) {
          event.preventDefault();
        }
      }
    });

    $rootScope.$on('$stateChangeSuccess', function() {
      $timeout(function () {
        $rootScope.showGrid = true;
        $('.ui-grid-cell').css('height', 'auto'); //jshint ignore:line
      }, 500);
    });

    $rootScope.resetGridHeight = function(delay) {
      if ($rootScope.gridResetTimer) {
        return;
      }
      if (!delay) {
        delay = 500;
      }
      $rootScope.gridResetTimer = $timeout(function () {
        $rootScope.showGrid = true;
        $('.ui-grid-cell').css('height', 'auto'); //jshint ignore:line
        $rootScope.gridResetTimer = null;
      }, delay);
    };

    $rootScope.logout = function() {
      UserService.logout().then(function () {
        $rootScope.user = null;
        $state.go('login');
      }, function (reason) {
        $log.error('Error on logout: '+angular.toJson(reason.data));
        $rootScope.user = null;
        $state.go('login');
      });
    };

}]);