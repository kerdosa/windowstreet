/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the angularjs controller for admin page.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


angular.module('sunshotApp')
.controller('AdminController', ['$scope', '$state', 'uiGridConstants', 'UserService', 'pagedUsers',
  function ($scope, $state, uiGridConstants, UserService, pagedUsers) {
    $scope.users = pagedUsers.results;

    $scope.tabs = [
      {name: 'user', heading: 'Users', active: true},
      {name: 'system', heading: 'Systems'},
      {name: 'endowment', heading: 'Endowments'},
      {name: 'borrower', heading: 'Borrowers'},
      {name: 'installer', heading: 'Installers'}
    ];

    var onetimeParams = null;

    $scope.tabSelect = function(tab) {
      $state.go('portal.admin.'+tab.name, onetimeParams);
      onetimeParams = null;
    };

    $scope.setActiveTab = function(name, params) {
      var foundTab = _.find($scope.tabs, function (tab) {   //jshint ignore:line
        return tab.name === name;
      });
      if (foundTab) {
        onetimeParams = params;
        foundTab.active = true;
      }
    };

    $scope.deactivateTab = function() {
      var activeTab = _.find($scope.tabs, function (tab) {  //jshint ignore:line
        return tab.active;
      });
      if (activeTab) {
        activeTab.active = false;
      }
    };

    // ui-grid min/max range filter for number field
    $scope.rangeFilter = [{
      condition: uiGridConstants.filter.GREATER_THAN_OR_EQUAL,
      placeholder: 'greater or equal'
    }, {
      condition: uiGridConstants.filter.LESS_THAN_OR_EQUAL,
      placeholder: 'less or equal'
    }];

}]);