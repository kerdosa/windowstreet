/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This contains the angularjs controllers for all model resources.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var appControllers = angular.module('sunshotApp');


var EDIT_TEMPLATE = '<button type="button" class="btn btn-xs btn-primary cell-action" data-ng-click="grid.appScope.edit(row)" title="Edit">Edit</button>';
var DELETE_TEMPLATE = '<button type="button" class="btn btn-xs btn-warning cell-action" data-ng-click="grid.appScope.delete(row)" title="Delete">Del</button>';

// Modal controller for viewing, editing and creating new entity
appControllers.controller('EntityModalController', ['$scope', '$modalInstance', 'operation', 'entityName', 'entity', 'USER_ROLE',
  function($scope, $modalInstance, operation, entityName, entity, USER_ROLE) {
    $scope.forms = {};
    $scope.submitted = false;
    $scope.operation = operation;
    $scope.entityName = entityName;
    $scope.tempEntity = operation === 'Edit' ? _.clone(entity) : entity;  //jshint ignore:line
    $scope.readOnly = operation === 'Show' ? true : false;
    $scope.cancelButtonName = operation === 'Show' ? 'Close' : 'Cancel';

    $scope.roles = [];
    _.each(USER_ROLE, function (value, key) { //jshint ignore:line
      $scope.roles.push(value);
    });

    $scope.$watch('tempEntity', function() {
      $scope.submitted = false;
    }, true);

    $scope.ok = function () {
      $scope.submitted = true;

      // if form exists, then close only if form is valid
      if ($scope.forms.entityForm) {
        if ($scope.forms.entityForm.$valid) {
          $modalInstance.close($scope.tempEntity);
        }
      } else {
        $modalInstance.close($scope.tempEntity);
      }
    };

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };

    $scope.dateFormat = 'yyyy-MM-dd';
    $scope.dateOptions = {
      startingDay: 1
    };

}]);

// Modal controller for uploading csv file
appControllers.controller('UploadController', ['$scope', '$modalInstance', '$upload', 'system',
  function($scope, $modalInstance, $upload, system) {
    $scope.status = null;
    $scope.closeDisabled = false;

    $scope.upload = function (files) {
      if (files && files.length) {
        var file = files[0];    // one file at a time
        $scope.status = {
          percent: 0,
          message: 'uploading ' + file.name + ' ...',
          done: false
        };
        $scope.closeDisabled = true;

        $upload.upload({
            url: '/systemPerformances/upload',
            fields: {
                'systemId': system.id
            },
            file: file
        }).progress(function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $scope.status.percent = progressPercentage;
            
        }).success(function () {
            $scope.status.percent = 100;
            $scope.status.message = 'Uploaded successfully';
            $scope.closeDisabled = false;
        }).error(function (err) {
          $scope.status.message = 'Upload failed: ';
          if (err) {
            $scope.status.message += err.message;
          }
          $scope.closeDisabled = false;
        });
      }
    };

    $scope.close = function () {
      $modalInstance.dismiss();
    };

}]);

// User controller
appControllers
.controller('UserController', ['$scope', '$state', 'EntityUtils', 'toastr', 'User', 'pagedUsers',
  function ($scope, $state, EntityUtils, toastr, User, pagedUsers) {
    $scope.entityName = $state.current.data.entityName;
    $scope.users = pagedUsers.results;

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      columnDefs: [
        {name: 'email', width: '50%', filter: {placeholder: 'starts with'}},
        {name: 'role', width: '25%', filter: {placeholder: 'starts with'}},
        {name: 'action', enableFiltering: false, width: '25%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE}
      ],
      data: $scope.users,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });
      }
    });
    
    $scope.add = function() {
      $scope.entity = new User();
      EntityUtils.createEntityModal($scope, 'Add', $scope.entityName, $scope.entity, $scope.users, User);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Edit', $scope.entityName, $scope.entity, $scope.users, User);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', $scope.entityName, $scope.entity, $scope.users, User);
    };

}]);

// System controller
appControllers
.controller('SystemController', ['$scope', '$state', 'EntityUtils', '$filter', 'toastr', 'uiGridConstants', 'System', 'Borrower', 'Installer', 
    'pagedBorrowers', 'pagedInstallers', 'pagedSystems',
  function ($scope, $state, EntityUtils, $filter, toastr, uiGridConstants, System, Borrower, Installer, pagedBorrowers, pagedInstallers, pagedSystems) {
    $scope.entityName = $state.current.data.entityName;
    $scope.borrowers = pagedBorrowers.results;
    $scope.installers = pagedInstallers.results;

    _.each(pagedSystems.results, function (system) {  //jshint ignore:line
      EntityUtils.combineEntityFields($scope.entityName, system);
    });
    $scope.systems = pagedSystems.results;

    var CHILDREN_TEMPLATE = '<button type="button" class="btn btn-xs btn-info cell-action" data-ng-click="grid.appScope.gotoPerformance(row)" title="System Performances">Performances</button>';

    function installerCondition(searchTerm, cellValue) {
      if (cellValue) {
        return new RegExp('^'+searchTerm).test(cellValue.companyName);
      } else {
        return false;
      }
    }

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      columnDefs: [
        {name: 'borrower',  width: '12%', filter: {condition: EntityUtils.entityNameCondition, placeholder: 'starts with'},
          cellTemplate: '<a href="javascript:void(0);" data-ng-click="grid.appScope.showBorrower(row.entity.borrower)">{{row.entity.borrower.name}}&nbsp;</a>'},
        {name: 'installer',  width: '12%', filter: {condition: installerCondition, placeholder: 'starts with'}, 
          cellTemplate: '<a href="javascript:void(0);" data-ng-click="grid.appScope.showInstaller(row.entity.installer)">{{row.entity.installer.companyName}}&nbsp;</a>'},
        {name: 'combinedAddress', displayName: 'Address', width: '26%', filter: {condition: uiGridConstants.filter.CONTAINS, placeholder: 'contains'}},
        {name: 'latlng', displayName: 'Lat/Lng', width: '10%', filter: {condition: uiGridConstants.filter.CONTAINS, placeholder: 'contains'}},
        {name: 'installationDate', width: '10%', filter: {placeholder: 'starts with'}, cellTemplate: '<div>{{row.entity.installationDate | date: "yyyy-MM-dd"}}</div>'},
        {name: 'finishDate',  width: '10%', filter: {placeholder: 'starts with'}, cellTemplate: '<div>{{row.entity.finishDate | date: "yyyy-MM-dd"}}</div>'},
        {name: 'action', enableFiltering: false, width: '20%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE+CHILDREN_TEMPLATE}
      ],
      data: $scope.systems,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });
      }
    });

    $scope.showBorrower = function(borrower) {
      // show popup with borrower detail
      Borrower.get({id: borrower.id, _populate: true}, function (fullBorrower) {
        $scope.endowments = [fullBorrower.endowment];
        EntityUtils.createEntityModal($scope, 'Show', 'Borrower', fullBorrower, null, null);
      }, function (err) {
        toastr.error(err.data.message, 'Read Borrower failed');
      });
    };

    $scope.showInstaller = function(installer) {
      // show popup with installer detail
      Installer.get({id: installer.id, _populate: true}, function (fullInstaller) {
        EntityUtils.createEntityModal($scope, 'Show', 'Installer', fullInstaller, null, null);
      }, function (err) {
        toastr.error(err.data.message, 'Read Installer failed');
      });
    };

    $scope.gotoPerformance = function(row) {
      // remove active tab
      $scope.deactivateTab();
      // row entity is system
      $state.go('^.systemPerformance', {systemId: row.entity.id});
    };

    $scope.add = function() {
      $scope.entity = new System();
      EntityUtils.createEntityModal($scope, 'Add', $scope.entityName, $scope.entity, $scope.systems, System);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      // set borrower and installer from list
      $scope.entity.borrower = EntityUtils.findEntity($scope.borrowers, row.entity.borrower);
      $scope.entity.installer = EntityUtils.findEntity($scope.installers, row.entity.installer);
      EntityUtils.createEntityModal($scope, 'Edit', $scope.entityName, $scope.entity, $scope.systems, System);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', $scope.entityName, $scope.entity, $scope.systems, System);
    };

    // For System modal date-picker
    $scope.installationDateOpened = false;
    $scope.finishDateOpened = false;

    $scope.openInstallationDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.installationDateOpened = true;
    };
    $scope.openFinishDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.finishDateOpened = true;
    };

}]);

// SystemPerformance controller
appControllers
.controller('SystemPerformanceController', ['$scope', '$state', '$modal', 'EntityUtils', '$timeout', 'toastr', 'uiGridConstants', 'System', 'SystemPerformance', 'system', 'pagedPerformances',
  function ($scope, $state, $modal, EntityUtils, $timeout, toastr, uiGridConstants, System, SystemPerformance, system, pagedPerformances) {
    $scope.entityName = $state.current.data.entityName;
    $scope.system = system;
    $scope.pagedPerformances = pagedPerformances;
    $scope.performances = pagedPerformances.results;

    $scope.filterParams = {};
    $scope.pagingOptions = {
      pageNumber: 1,
      pageSize: $scope.GRID_OPTIONS.paginationPageSize
    };

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      // use external filtering, paging and sorting
      useExternalFiltering: true,
      useExternalPagination: true,
      useExternalSorting: true,
      columnDefs: [
        {name: 'dailyEnergy', displayName: 'Daily Energy(kWh)', width: '20%', filters: angular.copy($scope.rangeFilter), cellTemplate: '<div>{{row.entity.dailyEnergy}} kWh</div>'},
        {name: 'systemACP', displayName: 'System ACP', width: '15%', filters: angular.copy($scope.rangeFilter), cellTemplate: '<div>{{row.entity.systemACP}} kW</div>'},
        {name: 'systemDCP', displayName: 'System DCP', width: '15%', filters: angular.copy($scope.rangeFilter), cellTemplate: '<div>{{row.entity.systemDCP}} kW</div>'},
        {name: 'systemDCV', displayName: 'System DCV', width: '15%', filters: angular.copy($scope.rangeFilter), cellTemplate: '<div>{{row.entity.systemDCV}} kW</div>'},
        {name: 'systemDCA', displayName: 'System DCA', width: '15%', filters: angular.copy($scope.rangeFilter), cellTemplate: '<div>{{row.entity.systemDCA}} kW</div>'},
        {name: 'action', enableFiltering: false, width: '24%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE}
      ],
      data: $scope.performances,
      totalItems: $scope.pagedPerformances.total,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });

        // handle paging externally
        $scope.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          $scope.pagingOptions.pageNumber = newPage;
          $scope.pagingOptions.pageSize = pageSize;
          getPerformancesData(0);
        });

        // handle filtering externally
        $scope.gridApi.core.on.filterChanged($scope, function () {
          // update filter values
          updateFilterValues(this.grid.columns);
          // get new performance data
          getPerformancesData($scope.FILTER_QUERY_DELAY);
        });
      }
    });

    $scope.back = function() {
      $scope.setActiveTab('system');
    };

    $scope.add = function() {
      $scope.entity = new SystemPerformance({
        system: $scope.system.id,
      });
      EntityUtils.createEntityModal($scope, 'Add', $scope.entityName, $scope.entity, $scope.performances, SystemPerformance);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Edit', $scope.entityName, $scope.entity, $scope.performances, SystemPerformance);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', $scope.entityName, $scope.entity, $scope.performances, SystemPerformance);
    };

    $scope.upload = function() {
      var templateUrl = 'partials/popups/upload.html';

      $modal.open({
        templateUrl: templateUrl,
        controller: 'UploadController',
        scope: $scope,
        size: 'sm',
        resolve: {
          SystemPerformance: function () {
            return SystemPerformance;
          },
          system: function () {
            return system;
          }
        }
      });
    };

    // For SystemPerformance modal date-picker
    $scope.performanceDateOpened = false;
    $scope.openPerformanceDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.performanceDateOpened = true;
    };

    function updateFilterValues(gridColumns) {
      var filterParams = {};
      // for each column, get min/max filter values & build query parameters
      _.each(_.range(4), function (index) { //jshint ignore:line
        var column = gridColumns[index];
        var min = column.filters[0].term;   // this is string
        var max = column.filters[1].term;
        var queryValue = '';
        if (min && !_.isNaN(Number(min))) {   //jshint ignore:line
          queryValue = min;
        }
        if (max && !_.isNaN(Number(max))) {   //jshint ignore:line
          queryValue += ','+max;
        }
        if (queryValue) {
          filterParams[column.field] = queryValue;
        }
      });
      $scope.filterParams = filterParams;
    }

    function getPerformancesData(delay) {
      if ($scope.timer) {
        $timeout.cancel($scope.timer);
        $scope.timer = null;
      }
      $scope.timer = $timeout(function () {
        var params = angular.copy($scope.filterParams);  // deep copy
        params._pageNumber = $scope.pagingOptions.pageNumber;
        params._pageSize = $scope.pagingOptions.pageSize;
        SystemPerformance.query(params, function (pagedPerformances) {
          $scope.pagedPerformances = pagedPerformances;
          $scope.performances = pagedPerformances.results;
          $scope.gridOptions.data = $scope.performances;
          $scope.gridOptions.totalItems = $scope.pagedPerformances.total;
        }, function (err) {
          toastr.error(err.data.message, 'Read SystemPerformance failed');
        });
      }, delay);
    }

}]);



// Endowment controller
appControllers
.controller('EndowmentController', ['$scope', '$state', 'EntityUtils', 'toastr', 'uiGridConstants', 'Endowment', 'User',  'pagedEndowments',
  function ($scope, $state, EntityUtils, toastr, uiGridConstants, Endowment, User, pagedEndowments) {
    $scope.entityName = $state.current.data.entityName;
    $scope.endowments = pagedEndowments.results;

    var CHILDREN_TEMPLATE = '<button type="button" class="btn btn-xs btn-info cell-action" data-ng-click="grid.appScope.gotoBorrower(row)" title="Borrowers">Borrowers</button>';

    function userEmailCondition(searchTerm, cellValue) {
      if (cellValue) {
        return new RegExp('^'+searchTerm, 'i').test(cellValue.email);
      } else {
        return false;
      }
    }

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      columnDefs: [
        {name: 'user', width: '40%', filter: {condition: userEmailCondition, placeholder: 'starts with'}, cellTemplate: '<a href="javascript:void(0);" data-ng-click="grid.appScope.showUser(row.entity.user)">{{row.entity.user.email}}&nbsp;</a>'},
        {name: 'name', width: '30%', filter: {placeholder: 'starts with'}},
        {name: 'action', enableFiltering: false, width: '30%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE+CHILDREN_TEMPLATE}
      ],
      data: $scope.endowments,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });
      }
    });

    $scope.gotoBorrower = function(row) {
      // row entity is endowment
      var endowment = row.entity;
      $scope.setActiveTab('borrower', {endowmentId: endowment.id});
    };

    $scope.showUser = function(user) {
      User.get({id: user.id, _populate: true}, function (fullUser) {
        EntityUtils.createEntityModal($scope, 'Show', 'User', fullUser, null, null);
      }, function (err) {
        toastr.error(err.data.message, 'Read User failed');
      });
    };

    $scope.add = function() {
      $scope.entity = new Endowment();
      EntityUtils.createEntityModal($scope, 'Add', $scope.entityName, $scope.entity, $scope.endowments, Endowment);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      $scope.entity.user = EntityUtils.findEntity($scope.users, row.entity.user);
      EntityUtils.createEntityModal($scope, 'Edit', $scope.entityName, $scope.entity, $scope.endowments, Endowment);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', $scope.entityName, $scope.entity, $scope.endowments, Endowment);
    };

}]);

// Installer controller
appControllers
.controller('InstallerController', ['$scope', '$state', 'EntityUtils', 'toastr', 'uiGridConstants', 'Installer', 'pagedInstallers',
  function ($scope, $state, EntityUtils, toastr, uiGridConstants, Installer, pagedInstallers) {
    $scope.entityName = $state.current.data.entityName;
    $scope.installers = pagedInstallers.results;

    _.each($scope.installers, function (installer) {  //jshint ignore:line
      EntityUtils.combineEntityFields($scope.entityName, installer);
    });

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      columnDefs: [
        {name: 'companyName', displayName: 'Company', width: '12%', filter: {placeholder: 'starts with'}},
        {name: 'contactName', displayName: 'Contact', width: '10%', filter: {placeholder: 'starts with'}},
        {name: 'email', width: '15%', filter: {placeholder: 'starts with'}},
        {name: 'phoneNumber', displayName: 'Phone', width: '7%', filter: {placeholder: 'starts with'}},
        {name: 'numberOfEmployees', displayName: '# of Employee', width: '7%', filter: {placeholder: 'starts with'}},
        {name: 'geographicalServiceArea', displayName: 'Service Area', width: '10%', filter: {placeholder: 'starts with'}},
        {name: 'combinedAddress', displayName: 'Address', width: '27%', filter: {condition: uiGridConstants.filter.CONTAINS, placeholder: 'contains'}},
        {name: 'action', enableFiltering: false, width: '15%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE}
      ],
      data: $scope.installers,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });
      }
    });

    $scope.add = function() {
      $scope.entity = new Installer();
      EntityUtils.createEntityModal($scope, 'Add', $scope.entityName, $scope.entity, $scope.installers, Installer);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Edit', $scope.entityName, $scope.entity, $scope.installers, Installer);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', $scope.entityName, $scope.entity, $scope.installers, Installer);
    };

}]);

// Borrower Controller
appControllers
.controller('BorrowerController', ['$scope', '$state', 'EntityUtils', 'toastr', 'uiGridConstants', 'Borrower', 'Endowment', 'pagedEndowments', 'pagedBorrowers',
  function ($scope, $state, EntityUtils, toastr, uiGridConstants, Borrower, Endowment, pagedEndowments, pagedBorrowers) {
    $scope.entityName = $state.current.data.entityName;
    $scope.endowments = pagedEndowments.results;
    $scope.borrowers = pagedBorrowers.results;

    _.each($scope.borrowers, function (borrower) {  //jshint ignore:line
      EntityUtils.combineEntityFields($scope.entityName, borrower);
    });

    var CHILDREN_TEMPLATE = '<button type="button" class="btn btn-xs btn-info cell-action" data-ng-click="grid.appScope.gotoLoan(row)" title="Loans">Loans</button><button type="button" class="btn btn-xs btn-info cell-action" data-ng-click="grid.appScope.gotoSystem(row)" title="Systems">Systems</button>';

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      columnDefs: [
        {name: 'endowment',  width: '10%', filter: {condition: EntityUtils.entityNameCondition, placeholder: 'starts with'},
          cellTemplate: '<a href="javascript:void(0);" data-ng-click="grid.appScope.showEndowment(row.entity.endowment)">{{row.entity.endowment.name}}&nbsp;</a>'},
        {name: 'name', width: '10%', filter: {placeholder: 'starts with'}},
        {name: 'email', width: '20%', filter: {placeholder: 'starts with'}},
        {name: 'phoneNumber', displayName: 'Phone', width: '10%', filter: {placeholder: 'starts with'}},
        {name: 'combinedAddress', displayName: 'Address', width: '25%', filter: {condition: uiGridConstants.filter.CONTAINS, placeholder: 'contains'}},
        {name: 'action', enableFiltering: false, width: '20%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE+CHILDREN_TEMPLATE}
      ],
      data: $scope.borrowers,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });
      }
    });

    $scope.gotoLoan = function(row) {
      // remove active tab
      $scope.deactivateTab();
      // row entity is borrower
      $state.go('^.loan', {borrowerId: row.entity.id});
    };

    $scope.gotoSystem = function(row) {
      // row entity is borrower
      var borrower = row.entity;
      $scope.setActiveTab('system', {borrowerId: borrower.id});
    };

    $scope.showEndowment = function(endowment) {
      Endowment.get({id: endowment.id, _populate: true}, function (fullEndowment) {
        fullEndowment.user = EntityUtils.findEntity($scope.users, fullEndowment.user);
        EntityUtils.createEntityModal($scope, 'Show', 'Endowment', fullEndowment, null, Endowment);
      }, function (err) {
        toastr.error(err.data.message, 'Read Endowment failed');
      });
    };

    $scope.add = function() {
      $scope.entity = new Borrower();
      EntityUtils.createEntityModal($scope, 'Add', $scope.entityName, $scope.entity, $scope.borrowers, Borrower);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      $scope.entity.endowment = EntityUtils.findEntity($scope.endowments, row.entity.endowment);
      EntityUtils.createEntityModal($scope, 'Edit', $scope.entityName, $scope.entity, $scope.borrowers, Borrower);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', $scope.entityName, $scope.entity, $scope.borrowers, Borrower);
    };

}]);

// Loan controller
appControllers
.controller('LoanController', ['$scope', 'EntityUtils', '$state', 'toastr', 'uiGridConstants', 'Borrower', 'Loan', 'borrower', 'pagedLoans',
  function ($scope, EntityUtils, $state, toastr, uiGridConstants, Borrower, Loan, borrower, pagedLoans) {
    var entityName = $state.current.data.entityName;
    $scope.borrower = borrower;
    $scope.loans = pagedLoans.results;

    var CHILDREN_TEMPLATE = '<button type="button" class="btn btn-xs btn-info cell-action" data-ng-click="grid.appScope.gotoPayment(row)" title="Loan Payments">Payments</button>';

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      columnDefs: [
        {name: 'desiredLoanTerm', width: '10%', filters: angular.copy($scope.rangeFilter)},
        {name: 'householdAGI', width: '15%', filters: angular.copy($scope.rangeFilter)},
        {name: 'monthlyExpenses', width: '10%', filters: angular.copy($scope.rangeFilter)},
        {name: 'loanTotal', width: '16%', filters: angular.copy($scope.rangeFilter)},
        {name: 'automaticPayments', width: '7%', filter: {placeholder: 'starts with'}, cellTemplate: '<div>{{row.entity.automaticPayments ? "Yes" : "No"}}</div>'},
        {name: 'interestRate', width: '7%', filters: angular.copy($scope.rangeFilter)},
        {name: 'loanDate',  width: '12%', filter: {placeholder: 'starts with'}, cellTemplate: '<div>{{row.entity.loanDate | date: "yyyy-MM-dd"}}</div>'},
        {name: 'action', enableFiltering: false, width: '23%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE+CHILDREN_TEMPLATE}
      ],
      data: $scope.loans,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });
      }
    });

    $scope.gotoPayment = function(row) {
      // row entity is loan
      $state.go('^.loanPayment', {loanId: row.entity.id});
    };

    $scope.back = function() {
      // back to borrower tab
      $scope.setActiveTab('borrower');
    };

    $scope.add = function() {
      $scope.entity = new Loan({borrower: $scope.borrower.id});
      EntityUtils.createEntityModal($scope, 'Add', entityName, $scope.entity, $scope.loans, Loan);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Edit', entityName, $scope.entity, $scope.loans, Loan);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', entityName, $scope.entity, $scope.loans, Loan);
    };

    // For Loan modal date-picker
    $scope.loanDateOpened = false;
    $scope.openLoanDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.loanDateOpened = true;
    };

}]);

// Loan Payment controller
appControllers
.controller('LoanPaymentController', ['$scope', 'EntityUtils', '$state', 'toastr', 'Loan', 'LoanPayment', 'loan', 'pagedLoanPayments',
  function ($scope, EntityUtils, $state, toastr, Loan, LoanPayment, loan, pagedLoanPayments) {
    $scope.entityName = $state.current.data.entityName;
    $scope.loan = loan;
    $scope.loanPayments = pagedLoanPayments.results;

    $scope.gridOptions = angular.extend({}, $scope.GRID_OPTIONS, {
      columnDefs: [
        {name: 'paymentAmount', width: '20%', filters: angular.copy($scope.rangeFilter)},
        {name: 'lateFees', width: '20%', filters: angular.copy($scope.rangeFilter)},
        {name: 'paymentDate',  width: '30%', filter: {placeholder: 'starts with'}, cellTemplate: '<div>{{row.entity.paymentDate | date: "yyyy-MM-dd"}}</div>'},
        {name: 'action', enableFiltering: false, width: '30%', cellTemplate: EDIT_TEMPLATE+DELETE_TEMPLATE}
      ],
      data: $scope.loanPayments,
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.filterChanged($scope, function () {
          $scope.resetGridHeight(200);
        });
      }
    });

    $scope.back = function() {
      // back to loan
      $state.go('^.loan', {borrowerId: $scope.loan.borrower});
    };

    $scope.add = function() {
      $scope.entity = new LoanPayment({loan: $scope.loan.id});
      EntityUtils.createEntityModal($scope, 'Add', $scope.entityName, $scope.entity, $scope.loanPayments, LoanPayment);
    };

    $scope.edit = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Edit', $scope.entityName, $scope.entity, $scope.loanPayments, LoanPayment);
    };

    $scope.delete = function(row) {
      $scope.entity = row.entity;
      EntityUtils.createEntityModal($scope, 'Delete', $scope.entityName, $scope.entity, $scope.loanPayments, LoanPayment);
    };

    // For LoanPayment modal date-picker
    $scope.paymentDateOpened = false;
    $scope.openPaymentDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.paymentDateOpened = true;
    };

}]);
