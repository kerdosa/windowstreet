/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the angularjs controller for login page.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


angular.module('sunshotApp')

// Login controller
.controller('LoginController', ['$rootScope', '$scope', '$state','toastr', 'UserService',
  function ($rootScope, $scope, $state, toastr, UserService) {
    $scope.status = {
      error: false,
      message: ''
    };

    $scope.login = function() {
      UserService.login($scope.email, $scope.password).then(function (result) {
        $rootScope.user = result.data;
        toastr.success('Signed in successfully', 'Success');
        if ($rootScope.user.role === 'Endowment') {
          $state.go('portal.landing');
        } else {
          $state.go('portal.admin.user');
        }
      }, function(reason) {
        toastr.error(reason.data.message, 'Login Failed');
      });
    };
}]);