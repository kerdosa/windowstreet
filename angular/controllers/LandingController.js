/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the angularjs controller for landing page.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

angular.module('sunshotApp')
  .controller('LandingController', ['$scope', 'landingData', 
    function ($scope, landingData) {
      $scope.landingData = landingData;
}]);