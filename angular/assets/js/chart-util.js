/*! ui-prototype - v0.1.1
* Copyright (c) 2015 Topcoder*
* ========================================================================
* author: Shrikanth Rajarajan
* version: 0.1.1
* ======================================================================== */

//Chart global options
Chart.defaults.global.responsive = true;
Chart.defaults.global.maintainAspectRatio = true;
Chart.defaults.global.showTooltips = false;
Chart.defaults.global.scaleFontFamily = "Montserrat";
Chart.defaults.global.scaleBeginAtZero = false;

//Set chart.js options for normal line chart
var options = {
  bezierCurve : false,
  pointDotRadius : 6,
  pointDot : true,
  scaleFontSize: 12,
  datasetStrokeWidth : 5,
  datasetFill : false,
  scaleShowVerticalLines: false
};

//set options for bezier filled curved chart(line)
var options_bezier = {
  pointDot : false,
  scaleFontSize: 18,
  datasetStrokeWidth: 1,
  datasetFill : true,
  bezierCurve : true,
  scaleShowVerticalLines: false
};

/* Takes the chart element, data, id of the new chart created, height of the chart and type of chart
   Creates new chart
   Create the chart after the div is expanded which is 0.3s delay so that chart takes up 100% of width
 */
var createChart = function(chart_el, data, id, height, curved) {
  if(isSmallScreen() && !curved) {
    height = height * 2;
  }
  var chart = $("<canvas id='" + id.replace("#", "") + "' height='"+height+"' class='full-chart'></canvas>"); //chart element
  $(chart_el).html(chart); //set chart element
  chart.parent($(chart_el));
  /* setTimeout to 300ms delay */
  setTimeout(function(){
    var ctx = $(id).get(0).getContext("2d");
    if(curved) {
      return new Chart(ctx).Line(data, options_bezier);
    } else {
      return new Chart(ctx).Line(data, options);
    }
  }, 300);
};

/* 
  Load Timeline with Labels from Json
  Parse JSON and set the Labels
  Takes in data and id to set the labels
  Label here is the one in the Slider
*/
var createTimeline = function(data, id) {
  var labels = $(id + " .timeline .labels");
  labels.html("");
  $(id + " .expand-widget").addClass("hidden");
  labels.parent().removeClass("hidden");
  for(label of data.sliderLabels) {
    labels.append("<span style='width:" + 100/(data.sliderLabels.length+1)+"%'>"+label+"</span>");
  }
};

/*
  Populate chart data and labels.
 */
var populateChartData = function(chartConfig, data, nextData, label) {
  var keys = [], values = [], sliderLabels = [];
  _.each(data, function (item, index) {
    _.each(item, function (value, key) {
      if (label && label === 'yearly') {
        var parts = key.split('/');
        if (parts[0] === '1') { // only put January
          keys.push('Jan '+parts[1]);
        }
      } else if (label && label === 'no') {   // no label
        keys.push('');
      } else {    // only put every 4 months
        if ((index % 4) === 0) {
          keys.push(key);
        } else {
          keys.push('');
        }
      }
      values.push(value);
    });
  });
  
  var chartData = _.clone(chartConfig, true);
  chartData.labels = keys;
  chartData.datasets[0].data = values;

  if (nextData) {
    for (var i=1; i<=4; i++) {
      sliderLabels.push('January '+(Number(nextData)+i));
    }
    chartData.sliderLabels = sliderLabels;
  }
  return chartData;
}

function getNextDataParameter(index) {
  // 4 years at index 0 is from current year - 3 to current year, so nextData is current year - 4
  var year = new Date().getFullYear() - 4;
  year += index;
  return year;
}

/* Takes in chart_type and req_type as params
   req_type is whether to proceed next or previous in the dataset array
*/
var loadChart = function(chart_type, req_type) {
  var index = parseInt($(chart_type + " .chart").attr("data-index"));  
  //Alter value of index based on req_type
  if(req_type.toString() === "default") {
    index = 0;
  } else if(req_type.toString() === "previous") {
    index--;
  } else if(req_type.toString() === "next") {
    index++;
  }
  var nextData = getNextDataParameter(index);

  var url, chartConfig;
  if(chart_type === '#total-clean-energy') {
    url = $base_url + '/cleanEnergy?nextData='+nextData;
    chartConfig = cleanEnergyChartConfig;
  } else if(chart_type === '#total-systems-deployed') {
    url = $base_url + '/installedSystem?nextData='+nextData;
    chartConfig = installedSystemChartConfig;
  }
  //Fetch JSON parse it and load it to chart and timeline slider
  $.getJSON(url, function (data) {
    var chartData = populateChartData(chartConfig, data, nextData);
    createTimeline(chartData, chart_type);
    createChart(chart_type+" .chart", chartData, chart_type+"-chart", 75, false);
    $(chart_type + " .chart").attr("data-index", index);
  });
};

//Whether screen size is less than 992px
var isSmallScreen = function () {
  return $(window).width() <= 992;
};