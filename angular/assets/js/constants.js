/*! ui-prototype - v0.1.1
* Copyright (c) 2015 Topcoder*
* ========================================================================
* author: Shrikanth Rajarajan
* version: 0.1.1
* ======================================================================== */
var $base_url = "/dashboard";

var cleanEnergyChartConfig = {
  "datasets":[
    {
      "label":"Total Clean Energy 04",
      "strokeColor":"#fed16a",
      "pointColor":"#d58f44",
      "pointStrokeColor":"#e9e9e9",
      "pointHighlightFill":"#fff",
      "pointHighlightStroke":"rgba(220,220,220,1)"
    }
  ]
}

var installedSystemChartConfig = {
  "datasets":[
    {
      "label":"Total Systems Deployed 04",
      "strokeColor":"#fed16a",
      "pointColor":"#d58f44",
      "pointStrokeColor":"#e9e9e9",
      "pointHighlightFill":"#fff",
      "pointHighlightStroke":"rgba(220,220,220,1)"
    }
  ]
}

var carbonImpactChartConfig = {
  "datasets": [{
    "label": "Total Carbon Impact",
    "fillColor": "#97bb3d",
    "strokeColor": "#97bb3d",
  }]
};

var moneyInvestedChartConfig = {
  "datasets": [{
    "label":"My First dataset",
    "fillColor":"#8eca3f",
    "strokeColor":"#8eca3f",
  }]
};