/*! ui-prototype - v0.1.1
* Copyright (c) 2015 Topcoder*
* ========================================================================
* author: Shrikanth Rajarajan
* version: 0.1.1
* ======================================================================== */

$(document).ready(function (){

  $(".expand-widget").click(function (e) {
    e.preventDefault();
    var target = $(this).data("target");
    $(this).parents().eq(3).removeClass("col-lg-6").addClass("col-lg-12");
    
    if(target === "#total-carbon-impact") {
      $(target + " .stat").addClass("col-lg-5");
      $(target + " .chart").addClass("col-lg-6");
      $.getJSON($base_url + '/carbonImpact', function (data) {
        var chartData = populateChartData(carbonImpactChartConfig, data, null, 'yearly');
        createChart(target +" .chart", chartData, target + "-chart", 100, true);
      });
      $(target + " .view-details").css('padding', 10);
      $(target + " .view-details").html("<h4 class='chart-note'>Note: All emission estimates from Calculation Fuel Usage</h4>");
    } else {
      loadChart(target, "default");
      if(target === "#total-systems-deployed") {
        initialize();
      }
    }
  });

  $(".previous-data").click(function (e) {
    e.preventDefault();
    var target = $(this).data("target");
    loadChart(target, "previous");
  });

  $(".next-data").click(function (e) {
    e.preventDefault();
    var target = $(this).data("target");
    loadChart(target, "next");
  });

  //Fetch JSON on total money invested and load it to bar chart
  $("#total-money-invested .view-details a").click(function (e) {
    e.preventDefault();
    var chart = $("<h5 class='chart-title'>Frequency of Investment <span>(in USD)</span></h5><canvas id='total-money-invested-chart' height='60' class='full-chart'></canvas>");
    $("#total-money-invested .view-details").html(chart);
    $(".return-rate").css("padding", 20);
    var ctx = $("#total-money-invested-chart").get(0).getContext("2d");
    $.getJSON($base_url + "/moneyEarned" , function (data) {
      var chartData = populateChartData(moneyInvestedChartConfig, data, null, 'no');
      new Chart(ctx).Bar(chartData, {
        responsive: true,
        scaleBeginAtZero : true,
        scaleShowVerticalLines: false
      });
    });
  });

});

/* tooltip manual invoke */
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});