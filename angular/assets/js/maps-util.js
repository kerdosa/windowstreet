/*! ui-prototype - v0.1.1
* Copyright (c) 2015 Topcoder*
* ========================================================================
* author: Shrikanth Rajarajan
* version: 0.1.1
* ======================================================================== */

//Initialize Google maps function
var initialize = function() {
  var map_canvas = $("#map-canvas-container");
  map_canvas.removeClass("hidden"); //show maps-container
  $.ajax({
      type: 'GET',
      dataType: 'json',
      data: {},
      url: $base_url + '/installedSystemLocation',
      error: function (jqXHR, textStatus, errorThrown) {
    },
    success: function (data) {
      // map_json = msg;
      var latlng = new google.maps.LatLng(data.center.latitude, data.center.longitude);   //set center for the map
      var myOptions = {
        zoom: 4,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
      for(system of data.systems){
        addMarker(system, map);
      }
    }
  });
};

// create marker info html with system object
function getSystemInfo(system) {
  var installDate = new Date(system.installationDate);
  var installDateStr = installDate.getFullYear()+'/'+installDate.getMonth()+'/'+installDate.getDate();
  var finishDate = new Date(system.finishDate);

  var info = '<div class="system-info">'+
    '<p><strong>Installer:</strong> '+system.installerCompanyName+'</p>'+
    '<p><strong>Borrower:</strong> '+system.borrowerName+'</p>'+
    '<hr>'+
    '<p><strong>Roof Type:</strong> '+system.roofType+'</p>'+
    '<p><strong>Annual Generation:</strong> '+system.projectedAnnualGeneration+'</p>'+
    '<p><strong>Inverter Type:</strong> '+system.solarInverterType+'</p>'+
    '<p><strong>Install Date:</strong> '+installDateStr+'</p>'+
    '</div>';
  return info;
}

//add a custom marker on the map
var addMarker = function(system, map) {
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(system.lat, system.lng),
    icon: "assets/img/marker.png",
    map: map
  });
  google.maps.event.addListener(marker, 'click', function() {
    var infowindow = new google.maps.InfoWindow({
        content: getSystemInfo(system)
    });
    infowindow.open(map, marker);
  });
};