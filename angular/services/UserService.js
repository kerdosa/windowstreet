/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the User service.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


angular.module('sunshotApp')
.factory('UserService', ['$http', '$q', function($http, $q) {
  var service = {};

  /**
   * Log in.
   */
  service.login = function(email, password) {
    return $http.post('/login', {email: email, password: password});
  };

  /**
   * Log out.
   */
  service.logout = function() {
    return $http.get('/logout');
  };

  /**
   * Get currently logged in user.
   */
  service.getUserMe = function() {
    var deferred = $q.defer();
    // prepare http request object
    var req = {
      method: 'GET',
      url: '/userme'
    };
    $http(req).then(function(payload) {
      deferred.resolve(payload.data);
    }, function(reason) {
      deferred.reject(reason);
    });
    return deferred.promise;
  };

  return service;
}]);

