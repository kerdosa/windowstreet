/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the Dashboard service.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


angular.module('sunshotApp')
.factory('DashboardService', ['$http', '$q', function($http, $q) {
  var DASHBOARD_URL = '/dashboard';

  function _getDashboardData(path) {
    var deferred = $q.defer();
    $http.get(DASHBOARD_URL+'/'+path).then(function (response) {
      deferred.resolve(response.data);
    }, function (reason) {
      deferred.reject(reason);
    });
    return deferred.promise;
  }


  function landing() {
    return _getDashboardData('landing');
  }

  function cleanEnergy() {
    return _getDashboardData('cleanEnergy');
  }

  function carbonImpact() {
    return _getDashboardData('carbonImpact');
  }

  function moneyEarned() {
    return _getDashboardData('moneyEarned');
  }

  function installedSystem() {
    return _getDashboardData('installedSystem');
  }
    
  return {
    landing: landing,
    cleanEnergy: cleanEnergy,
    carbonImpact: carbonImpact,
    moneyEarned: moneyEarned,
    installedSystem: installedSystem
  };
}]);


