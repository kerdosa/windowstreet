/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This contains the angularjs resources for all models.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

function transformToResource(Resource, pagedData) {
  try { 
    pagedData = JSON.parse(pagedData);
    var entities = [];
    _.each(pagedData.results, function (entity) { //jshint ignore:line
      entities.push(new Resource(entity));
    });
    pagedData.results = entities;
    return pagedData;
  } catch(e) {}
  return pagedData;
}

angular.module('sunshotApp')
.factory('User', ['$resource', function($resource) {
  var Resource = $resource('/users/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        params: {_pageSize: Number.MAX_SAFE_INTEGER},   // client-side paging & filtering
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}])
.factory('Endowment', ['$resource', function($resource) {
  var Resource = $resource('/endowments/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        params: {_pageSize: Number.MAX_SAFE_INTEGER, _populate: true},   // client-side paging & filtering
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}])
.factory('Borrower', ['$resource', function($resource) {
  var Resource = $resource('/borrowers/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        params: {_pageSize: Number.MAX_SAFE_INTEGER, _populate: true},   // client-side paging & filtering
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}])
.factory('Loan', ['$resource', function($resource) {
  var Resource = $resource('/loans/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        params: {_pageSize: Number.MAX_SAFE_INTEGER, _populate: true},   // client-side paging & filtering
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}])
.factory('LoanPayment', ['$resource', function($resource) {
  var Resource = $resource('/loanPayments/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        params: {_pageSize: Number.MAX_SAFE_INTEGER},   // client-side paging & filtering
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}])
.factory('Installer', ['$resource', function($resource) {
  var Resource = $resource('/installers/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        params: {_pageSize: Number.MAX_SAFE_INTEGER},   // client-side paging & filtering
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}])
.factory('System', ['$resource', function($resource) {
  var Resource = $resource('/systems/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        params: {_pageSize: Number.MAX_SAFE_INTEGER, _populate: true},   // client-side paging & filtering
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}])
.factory('SystemPerformance', ['$resource', function($resource) {   // server-side paging & filtering
  var Resource = $resource('/systemPerformances/:id',
    {id: '@id'},
    {
      query: {
        isArray: false,
        transformResponse: function (pagedData) {
          return transformToResource(Resource, pagedData);
        }
      }, 
      update: {method: 'PUT'}
    }
  );
  return Resource;
}]);

