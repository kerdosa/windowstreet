/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This contains the Utils methods for controller.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


angular.module('sunshotApp')
.factory('EntityUtils', ['$modal', 'toastr', function($modal, toastr) {

  /**
   * Combine address parts into one string.
   */
  function combineAddress(entity) {
    return entity.address+', '+entity.city+', '+entity.state+' '+entity.zipcode+', '+entity.country;
  }

  /**
   * Find an index of entity from entity array.
   */
  function findIndex(array, entity) {
    var index = _.findIndex(array, function (element) { //jshint ignore:line
      return element.id === entity.id;
    });
    return index;
  }

  /**
   * Find an entity from entity array.
   */
  function findEntity(array, entity) {
    var entity = _.find(array, function (element) { //jshint ignore:line
      return element.id === entity.id;
    });
    return entity;
  }

  /**
   * Combine entity fields depending on entity type.
   */
  function combineEntityFields(entityName, entity) {
    if (entityName === 'Installer' || entityName === 'Borrower' || entityName === 'System') {
      entity.combinedAddress = combineAddress(entity);
    }
    if (entityName === 'System') {
      entity.latlng = entity.lat+','+entity.lng;
    }
  }

  /**
   * Entity name filter condition.
   */
  function entityNameCondition(searchTerm, cellValue) {
    if (cellValue) {
      return new RegExp('^'+searchTerm, 'i').test(cellValue.name);
    } else {
      return false;
    }
  }

  /**
   * Create a modal popup for entity Add, Edit and Show operation.
   */
  function createEntityModal($scope, operation, entityName, entity, entities, resource) {
    var templateUrl;
    if (operation === 'Delete') {
      templateUrl = 'partials/popups/delete.html';
    } else {
      templateUrl = 'partials/popups/'+entityName.toLowerCase()+'.html';
    }

    var modalInstance = $modal.open({
      templateUrl: templateUrl,
      controller: 'EntityModalController',
      scope: $scope,
      size: (entityName === 'System' || entityName === 'Installer' || entityName === 'Borrower' || entityName === 'Loan' || entityName === 'SystemPerformance') && operation !== 'Delete' ? '' : 'sm',
      resolve: {
        operation: function () {
          return operation;
        },
        entityName: function () {
          return entityName;
        },
        entity: function () {
          return entity;
        }
      }
    });

    var entityHandlers = {
      Add: function(entityName, entity, entities) {
        // do create operation
        entity.$save(function(entity) {
          combineEntityFields(entityName, entity);

          // add new entity
          entities.push(entity);
        }, function(err) {
          toastr.error(err.data.message, 'Create failed');
        });
      },
      Edit: function(entityName, entity, entities, resource) {
        // do update operation
        resource.update(entity, function() {
          combineEntityFields(entityName, entity);
        }, function(err) {
          toastr.error(err.data.message, 'Update failed');
        });
      },
      Delete: function(entityName, entity, entities) {
        entity.$delete(function(entity) {
          // remove entity from entity collection
          var index = entities.indexOf(entity);
          entities.splice(index, 1);
        }, function(err) {
          toastr.error(err.data.message, 'Delete failed');
        });
      }
    };

    // handle modal: add, edit or delete
    modalInstance.result.then(function(returnedEntity) {
      if (operation === 'Edit') {
        angular.extend(entity, returnedEntity);
      }
      var handler = entityHandlers[operation];
      if (handler) {
        handler(entityName, returnedEntity, entities, resource);
      }
      if (operation === 'Add') {
        $scope.resetGridHeight(200);
      }
    });
    return modalInstance;
  }

  return {
    combineAddress: combineAddress,
    findIndex: findIndex,
    findEntity: findEntity,
    combineEntityFields: combineEntityFields,
    entityNameCondition: entityNameCondition,
    createEntityModal: createEntityModal
  };

}]);
