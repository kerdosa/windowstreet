/*
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Contains all applications routes.
 *
 * @author TCSASSEMBLY
 * @version 1.0Ï
 */
'use strict';

module.exports = {
  '/login': {
      post: {
          controller: 'UserController',
          method: 'login',
          public: true
      }
  },
  '/resetPasswordLink': {
      get: {
          controller: 'UserController',
          method: 'resetPasswordLink',
          public: true
      }
  },
  '/resetPassword': {
      post: {
          controller: 'UserController',
          method: 'resetPassword',
          public: true
      }
  },
  '/userme': {
      get: {
          controller: 'UserController',
          method: 'getUserMe'
      }
  },
  '/logout': {
      get: {
          controller: 'UserController',
          method: 'logout'
      }
  },
  '/dashboard/landing': {
      get: {
          controller: 'DashboardController',
          method: 'getLandingData'
      }
  },
  '/dashboard/cleanEnergy': {
      get: {
          controller: 'DashboardController',
          method: 'getCleanEnergyChartData'
      }
  },
  '/dashboard/carbonImpact': {
      get: {
          controller: 'DashboardController',
          method: 'getCarbonImpactChartData'
      }
  },
  '/dashboard/moneyEarned': {
      get: {
          controller: 'DashboardController',
          method: 'getMoneyEarnedChartData'
      }
  },
  '/dashboard/installedSystem': {
      get: {
          controller: 'DashboardController',
          method: 'getInstalledSystemChartData'
      }
  },
  '/dashboard/installedSystemLocation': {
      get: {
          controller: 'DashboardController',
          method: 'getInstalledSystemMapData'
      }
  },
  '/systemPerformances/upload': {
      post: {
          controller: 'PerformanceController',
          method: 'uploadFile'
      }
  }
};