/*
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * This module contains the default application configuration.
 *
 * @author TCSASSEMBLY
 * @version 1.0
 */
'use strict';


var path = require('path'),
  env = require('node-env-file');


function getEnv(name) {
  env(path.join(__dirname, '../.env'));
  if (!process.env.hasOwnProperty(name)) {
    throw new Error('Env setting: ' + name + ' is not configured!');
  }
  return process.env[name].trim();
}

module.exports = {
  MONGODB_URL: 'mongodb://localhost:27017/sunshot-dev',

  SERVER_PORT: process.env.PORT || 3000,
  SESSION_SECRET: 'secret',
  SECURITY: {
    salt: 'SECURITY_SALT',
    iterations: 13,
    keylen: 16
  },
  PASSWORD_MIN_LEN: 5,
  RANDOM_PASSWORD_LEN: 8,
  PAGE_SIZE: 10,
  MAIL: {
    service: 'Gmail',
    auth: {
      user: getEnv('EMAIL_ADDRESS'),
      pass: getEnv('EMAIL_PASSWORD')
    }
  },
  FROM_EMAIL: getEnv('EMAIL_ADDRESS')

};