/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the startup script for web server.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


var express = require('express'),
  _ = require('lodash'),
  config = require('config'),
  passport = require('passport'),
  winston = require('winston'),
  errors = require('./errors'),
  constant = require('./helpers/constant'),
  session = require('express-session'),
  ejs = require('ejs'),
  path = require('path'),
  multer = require('multer'),
  middlewares = require('./helpers/middlewares');


var app = express();
app.set('port', config.SERVER_PORT || 3000);

// bootstrap mongodb connection and models
require('./models');
var UserService = require('./services/UserService');

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  UserService.get(id, done);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', ejs.renderFile);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, 'angular')));
app.use(session({
  secret: config.SESSION_SECRET,
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(multer());

// load all routes
_.each(require('./config/routes'), function (verbs, url) {
  _.each(verbs, function (def, verb) {
    var actions = [];
    if (!def.public) {
      actions.push(function (req, res, next) {
        if (!req.user) {
          next(new errors.UnauthorizedError('User is not authorized'));
        } else {
          next();
        }
      });
    }
    var method = require('./controllers/' + def.controller)[def.method];
    if (!method) {
      throw new Error(def.method + ' is undefined');
    }
    actions.push(method);
    app[verb](url, actions);
  });
});

// load routes for model CRUD
require('./controllers/CrudController')(app);

// configure root path
app.get('/', function(req, res) {
  res.render('index', {
    user: _.pick(req.user, 'id', 'role', 'email')
  });
});

app.use(function (req, res) {
  res.status(constant.HTTP_NOT_FOUND).json(new errors.NotFoundError('route not found'));
});

app.use(middlewares.errorHandler);

// start the application
app.listen(app.get('port'), function () {
  winston.info('Express server listening on port ' + app.get('port'));
});