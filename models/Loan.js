/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines Loan model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  borrower: {type: Schema.ObjectId, ref: 'Borrower', required: true},
  desiredLoanTerm: Number,
  householdAGI: Number,
  monthlyExpenses: Number,
  loanTotal: Number,
  automaticPayments: Boolean,
  interestRate: Number,
  loanDate: Date
});