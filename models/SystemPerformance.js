/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines SystemPerformance model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  system: {type: Schema.ObjectId, ref: 'System', required: true},
  dailyEnergy: Number,
  systemACP: Number,
  systemDCP: Number,
  systemDCV: Number,
  systemDCA: Number,
  date: Date
});