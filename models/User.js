/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines User model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  email: {type: String, required: true, unique: true},
  passwordHash: {type: String, required: false},
  role: {type: String, required: true, 'enum': ['Admin', 'Endowment']}
});