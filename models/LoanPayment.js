/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines LoanPayment model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  loan: {type: Schema.ObjectId, ref: 'Loan', required: true},
  paymentAmount: Number,
  paymentDate: Date,
  lateFees: Number
});