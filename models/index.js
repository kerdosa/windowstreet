/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * Initialize and exports all application models.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var fs = require('fs'),
  config = require('config'),
  mongoose = require('mongoose'),
  mongoosePaginate = require('mongoose-paginate');

// connect to monogdb
var connect = function () {
  var options = {server: {socketOptions: {keepAlive: 1}}};
  mongoose.connect(config.MONGODB_URL, options);
};
connect();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);

// Bootstrap models
fs.readdirSync(__dirname).forEach(function (file) {
  if (file !== 'index.js') {
    var filename = file.split('.')[0];
    var schema = require(__dirname+'/' + filename);

    schema.plugin(mongoosePaginate);
    var model = mongoose.model(filename, schema);
    model.schema.options.minimize = false;
    model.schema.options.toJSON = {
      transform: function (doc, ret) {
          if (ret._id) {
            ret.id = String(ret._id);
            delete ret._id;
          }
          if (ret.passwordHash) {
            delete ret.passwordHash;
          }
          delete ret.__v;
          return ret;
      }
    };
  }
});
