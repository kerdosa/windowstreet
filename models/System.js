/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines System model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  borrower: {type: Schema.ObjectId, ref: 'Borrower', required: true},
  installer: {type: Schema.ObjectId, ref: 'Installer', required: true},
  address: String,
  city: String,
  state: String,
  zipcode: String,
  country: String,
  size: Number,
  roofType: String,
  projectedAnnualGeneration: Number,
  solarModuleManufacturer: String,
  solarInverterManufacturer: String,
  solarModuleRating: Number,
  solarInverterType: String,
  installationDate: Date,
  finishDate: Date,
  lat: Number,
  lng: Number
});