/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines Borrower model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  endowment: {type: Schema.ObjectId, ref: 'Endowment', required: true},
  name: {type: String, required: true},
  address: String,
  city: String,
  state: String,
  zipcode: String,
  country: String,
  email: String,
  phoneNumber: String
});