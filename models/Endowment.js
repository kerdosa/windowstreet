/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines Endowment model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  user: {type: Schema.ObjectId, ref: 'User', required: true},
  name: {type: String, required: true}
});