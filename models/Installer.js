/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */
/**
 * This defines Installer model.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = new Schema({
  companyName: {type: String, required: true},
  contactName: String,
  address: String,
  city: String,
  state: String,
  zipcode: String,
  country: String,
  email: String,
  phoneNumber: String,
  numberOfEmployees: Number,
  geographicalServiceArea: String
});