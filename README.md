SunShot - Windowstreet
===


## Setup

* [nodejs 0.12.x](https://nodejs.org/)
* [mongodb 3.0.0](https://www.mongodb.org/)
* [bower 1.4.1](http://bower.io)
* [grunt 0.1.13](http://gruntjs.com/)


## Configuration
All configuration is set up in `config/default.js`. The default configuration should work OK for development. To overwrite the configuration for production or test environment, add `config/production.js` or `config/test.js`.

* **MONGODB_URL** the MongoDB URL
* **SERVER_PORT** the web server port
* **SESSION_SECRET** the Express session secret
* **SECURITY** the configuration related to password hashing
    * **salt** the salt
    * **iterations** the iterations for pbkdf2 algorithm
    * **keylen** the key length for pbkdf2 algorithm
* **PAGE_SIZE** the default page size of get all query
* **PASSWORD_MIN_LEN** the minimum password length
* **RANDOM_PASSWORD_LEN** the length of random password generate for reset password
* **MAIL** the mail provider and credential
    * **service**: mail service provider, for example `Gmail`
    * **auth** mail service credential
    	* **user** email account of mail service, by default populated from EMAIL_ADDRESS environment variable
    	* **pass** password of mail service, by default populated from EMAIL_PASSWORD environment variable
* **FROM_EMAIL** the from email, by default populated from EMAIL_ADDRESS environment variable


The email templates for reset password is configured in `config/emailTemplates/RESET_PASSWORD`.

To test the reset password feature, a valid mail service and account should be configured. Please set email account and password in `env-sample` and copy it to `.env` file before starting the application.

Note on Gmail:

When using Gmail account first time, Google blocks signing in from the less secure app, and sends a warning email. Please click the link in the email and turn on `Access for less secure apps`.

## Model Modifications

To better represent the popuated reference model, the suffix `Id` was removed from reference field name. For example `borrowerId` is changed to `borrower` in the System model.

Depending on the _populate parameter, this field can be an ObjectId of reference object or reference object itself.

* ***_populate*** parameter is on: the reference field will have reference object.
	* `get all` request returns only _id and name field of reference object.
	* `get an entity by id` returns the whole reference object.
* ***_populate*** parameter is off: the reference field will have an ObjectId of reference object.


System.solarModuleRating is changed from String to Number type.

Loan.paymentDate is changed from String to Date type.

System.timestamp is removed, only System.date is used.

## Deployment

Install grunt and grunt-cli.

	$ sudo npm install -g grunt
	$ sudo npm install -g grunt-cli

Install bower

	$ sudo npm install -g bower

Unzip the submission and cd to the unzipped folder.

Install npm module dependencies and bower components.

	$ npm install

Run jslint test.

	$ grunt
	
Create test data to MongoDB. If there is out of memory error while running this script, please reduce the number of entities in the `create-data.js`. Specially decrease NUM_SYSTEMS_PER_INSTALLER or NUM_SYSTEM_WITH_DATA.

	$ cd test_files
	$ node create-data.js

This command populates three users.

	email: admin@gmail.com, password: password, role: Admin.
	email: topcoder3@gmail.com, password: password, role: Endowment.
	email: topcoder70@gmail.com, password: password, role: Endowment.

Start the application.

	$ npm start

By default the application runs in port 3000.



## Verification

Visit `http://localhost:3000`

Login as topcoder3@gmail.com/password, verify the chart landing page is opened.
Verify all charts works.

Login as admin@gmail.com/password, verify ***Users*** listing page is opened.

The admin page has five tabs, Users, Endowments, Systems, Borrowers, Installers.
In each tab, verify

* Field sorting
* Field filtering
* Paging
* Actions(New, Edit, Delete)
* Click the link of reference object, popup is displayed with the detail content of reference object.
* Click the children links, the child listing page is displayed.

In the children page, verify the back button goes back to the parent page.

In the System Performance:

* Verify the filtering/paging/sorting is done in the server-side.
* verify the upload works and creates records in DB. The CSV file should have the correct header. I modified `createTestCSV.js` to add the header. Please use the CSV file **test_files/test.csv** or **test_files/test-small.csv**, or regenerate it with `test_files/createTestCSV.js`. 

Reset password test:

* Note that a valid email account and provider should be configured to test this feature. See Configuration section how to configure them.
* Create new user in ***Users*** listing page.
* Verify an email with reset passwork link and dashboard link is sent to your email.
* Click the `reset passwork link`, verify the reset password page is displayed.
* Type new password and click `Reset Password` button, verify the login page is displayed.
* Log in with new password. Verify the Endowment user lands in Dashboard landing page, Admin user lands in the admin page.

## Heroku Deployment

Before you start make sure you have the [Heroku toolbelt](https://toolbelt.heroku.com/)
installed and an accessible MongoDB instance. Please see [mongolab in Heroku](https://devcenter.heroku.com/articles/mongolab#adding-mongolab) to get free mongolab instance in Heroku.

	$ git init
	$ git add .
	$ git commit -m "initial version"
	$ heroku apps:create
	$ heroku config:add NODE_ENV=production
		
	$ heroku addons:add mongolab
	$ heroku config | grep MONGOLAB_URI
	MONGOLAB_URI: mongodb://heroku_app35794496:utvajadltl6ps06......
	# copy MongoDB URI to MONGODB_URL in the config/production.js
	
	$ git add .
	$ git commit -m "set mongodb url"
	$ git push heroku master
	
	$ heroku run bash
	# this is Heroku shell.
	# this takes very long time in Heroku, so reduce the number of entities in the create-data.js before running.
	$ cd test_files
	$ node create-data.js
	# Crtl-D to exit from Heroku shell
	
	$ heroku open
	# log in as one of test users

Note that bower and moment are required for Heroku deployment, so they are moved to `dependencies` from `devDependencies`.
